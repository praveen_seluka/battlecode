package learn;

import battlecode.common.*;
import java.util.Random;

/**
 * Created by ps on 1/8/16.
 */
public class RobotPlayer {
    static Random rnd;

    static RobotController rc = null;
    static boolean isLeader = false;

    public static void run(RobotController xrc) {
        rc = xrc;
        rnd=new Random(rc.getID());
        //System.out.println(rc.getType()+ " "+rc.getID()+" "+rc.getRobotCount());
        if(rc.getType()==RobotType.ARCHON&&rc.getRoundNum()==0){
            Signal []s=rc.emptySignalQueue();
            if(s.length==0)
                isLeader=true;
            try {
                rc.broadcastMessageSignal(0,0,10);
            } catch (GameActionException e) {
                e.printStackTrace();
            }

        }
        while(true) {
            try {
                if (rc.getType() == RobotType.ARCHON) {
                    archon();
                } else if (rc.getType() == RobotType.SOLDIER) {
                    soldier();
                }
            }catch (GameActionException e) {
                e.printStackTrace();
            }
            Clock.yield();
        }
    }

    private static void soldier() throws GameActionException {
        Direction dest=randomDirection();
        attackSomeHostile();

        if(rc.isCoreReady() && rc.canMove(dest)) {
            try {
                rc.move(dest);
            } catch (GameActionException e) {
                e.printStackTrace();
            }
        }
    }

    public static void archon() throws GameActionException {
        Direction dest=randomDirection();
        if(rnd.nextInt(100)<80) attackSomeHostile();

        if(rc.isCoreReady()) {
            MapLocation[] ls = MapLocation.getAllMapLocationsWithinRadiusSq(rc.getLocation(), rc.getType().sensorRadiusSquared);
            for (MapLocation l : ls) {
                if(rc.senseParts(l)>0){
                    Direction target=rc.getLocation().directionTo(l);
                    if(rc.canMove(target)) rc.move(target);
                    break;
                }
            }
        }
        //build soldier
        if(rc.isCoreReady() && rc.canBuild(dest, RobotType.SOLDIER)) {
            rc.build(dest, RobotType.SOLDIER);
        }
        if(rc.isCoreReady()) {
            if(rc.canMove(dest)) rc.move(dest);
        }
    }
    private static void attackSomeHostile() throws GameActionException {

        if(rc.isCoreReady()){
            RobotInfo hostile[]=rc.senseHostileRobots(rc.getLocation(), rc.getType().sensorRadiusSquared);
            for(RobotInfo ri:hostile){
                double dist=rc.getLocation().distanceSquaredTo(ri.location);

                if (ri.weaponDelay > 1){
                    continue;
                }
                else {
                    if(dist<=8) {
                        Direction dest=rc.getLocation().directionTo(ri.location).opposite();
                        if(rc.canMove(dest)) {
                            rc.move(dest);
                            return;
                        }
                    }
                }
            }
            if(hostile.length>0){
                if(rc.isWeaponReady() && rc.canAttackLocation(hostile[0].location)) {
                    rc.attackLocation(hostile[0].location);
                }
            }
        }
    }

    private static Direction randomDirection() {
        return Direction.values()[(int)(rnd.nextDouble()*8)];
    }
}
