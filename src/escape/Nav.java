package escape;
import battlecode.common.*;
import java.util.Random;
/**
 * Created by ps on 1/11/16.
 */
public class Nav {
    static RobotController rc;
    static {
        rc=RobotPlayer.rc;
    }
    static MapLocation target;
    static void navigate(MapLocation target_in) throws GameActionException {
        if(rc.isCoreReady())
            navigate_(target_in);
    }
    static void navigate_(MapLocation target_in) throws GameActionException {
        target=target_in;
        Direction desiredDir=rc.getLocation().directionTo(target);
        if(rc.canMove(desiredDir)) {
            rc.move(desiredDir);
            return;
        }

        Direction to=flockInDir(desiredDir);
        if(to!=null && rc.canMove(to))
            rc.move(to);
        else {
            MapLocation loc=rc.getLocation().add(desiredDir);
            if(rc.senseRubble(loc)>0 && desiredDir!=Direction.OMNI) {
                rc.clearRubble(desiredDir);
            }
        }
    }
    static Direction flockInDir(Direction desiredDir){
        Direction[] directions = new Direction[3];
        directions[0] = desiredDir;
        Direction left = desiredDir.rotateLeft();
        Direction right = desiredDir.rotateRight();
        boolean leftIsBetter = (rc.getLocation().add(left).distanceSquaredTo(target) < rc.getLocation().add(right).distanceSquaredTo(target));
        directions[1] = (leftIsBetter ? left : right);
        directions[2] = (leftIsBetter ? right : left);


        for (int i = 0; i < directions.length; i++){
            if (rc.canMove(directions[i])){
                return directions[i];
            }
        }
        return null;
    }
}
