package combat;

/**
 * Created by ps on 1/12/16.
 */
public enum MessageType {

    NEUTRAL,
    PART,
    ZOMBIE_DEN,
    SOLDIER,
    CORNER;
    private MessageType() {
    }
}