package leader;

import battlecode.common.*;
import java.util.Random;

/**
 * Created by ps on 1/8/16.
 */
public class RobotPlayer {
    static Random rnd;

    static RobotController rc = null;
    static boolean isLeader = false;
    static int [] magneta = new int[]{238,0,238};

    public static void run(RobotController xrc) {
        rc = xrc;
        rnd=new Random(rc.getID());
        //System.out.println(rc.getType()+ " "+rc.getID()+" "+rc.getRobotCount());
        if(rc.getType()==RobotType.ARCHON&&rc.getRoundNum()==0){
            Signal []s=rc.emptySignalQueue();
            if(s.length==0) {
                isLeader = true;
                rc.setIndicatorDot(rc.getLocation().add(Direction.EAST,3),magneta[0],magneta[1],magneta[2]);
            }
            else if (s.length>0) {
                rc.setIndicatorString(0,s.length+" messages"+s[0].getMessage()[0]+" "+s[1].getMessage()[0]);
            }

            try {
                rc.broadcastMessageSignal(rc.getID(), 0, 1);
            } catch (GameActionException e) {
                e.printStackTrace();
            }

        }

        while(true) {
            try {
                if (rc.getType() == RobotType.ARCHON) {
                    archon();
                } else if (rc.getType() == RobotType.SOLDIER) {
                    soldier();
                }
            }catch (GameActionException e) {
                e.printStackTrace();
            }
            Clock.yield();
        }
    }

    private static void soldier() throws GameActionException {
        Direction dest=randomDirection();
        attackSomeHostile();
        doOrders();
        if(rc.isCoreReady() && rc.canMove(dest)) {
            try {
                rc.move(dest);
            } catch (GameActionException e) {
                e.printStackTrace();
            }
        }
    }

    public static void archon() throws GameActionException {
        Direction dest=randomDirection();

        repair();

        if(isLeader) {
            rc.broadcastMessageSignal(0,0,100);
            sendSignals();
        }

        if(rnd.nextInt(100)<80) {
            attackSomeHostile();
        }

        if(rc.isCoreReady()) {
            //TODO: read map xml to see neutral robot span time
            //TODO: convert the neutral robot
            //TODO: Activate the best available bot or the nearest one
            RobotInfo [] neutrals=rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, Team.NEUTRAL);
            if(neutrals.length>0) {
                if(rc.getLocation().isAdjacentTo(neutrals[0].location)) {
                    rc.activate(neutrals[0].location);
                    return;
                }
                Direction d=rc.getLocation().directionTo(neutrals[0].location);
                if(rc.canMove(d)){
                    rc.move(d);
                    return;
                }
            }

            MapLocation[] ls = MapLocation.getAllMapLocationsWithinRadiusSq(rc.getLocation(), rc.getType().sensorRadiusSquared);
            for (MapLocation l : ls) {
                if(rc.senseParts(l)>0){
                    Direction target=rc.getLocation().directionTo(l);
                    if(rc.canMove(target)) {
                        rc.move(target);
                        break;
                    }
                }
            }
        }
        //build soldier
        if (rc.isCoreReady() && rc.canBuild(dest, RobotType.SOLDIER)) {
            rc.build(dest, RobotType.SOLDIER);
        }


        if(rc.isCoreReady()) {
            if(rc.canMove(dest)) rc.move(dest);
        }
    }

    private static void repair() throws GameActionException {
        RobotInfo []ours=rc.senseNearbyRobots(rc.getType().attackRadiusSquared, rc.getTeam());
        for(RobotInfo our:ours) {
            if(our.type!=RobotType.ARCHON && our.health < our.type.maxHealth) {
                rc.repair(our.location);
                return;
            }
        }
    }

    private static void sendSignals() throws GameActionException {
        rc.broadcastMessageSignal(0,0,100);
    }
    private static void doOrders() throws  GameActionException {
        if(rc.isCoreReady()) {
            Signal[] msgs = rc.emptySignalQueue();
            for (Signal msg : msgs) {
                if (msg.getTeam() == rc.getTeam()) {
                    Direction towards = rc.getLocation().directionTo(msg.getLocation());
                    if (rc.canMove(towards)) {
                        rc.move(towards);
                        return;
                    }
                }

            }
        }
    }
    private static void attackSomeHostile() throws GameActionException {

        if(rc.isCoreReady()){
            //TODO: outnumbered enemy? then attack
            //TODO: dont attack zombie dens, instead run away
            RobotInfo hostile[]=rc.senseHostileRobots(rc.getLocation(), rc.getType().sensorRadiusSquared);
            //TODO: pick the best direction to run away, getRunawayDirections
            for(RobotInfo ri:hostile){
                double dist=rc.getLocation().distanceSquaredTo(ri.location);

                if (ri.weaponDelay > 1){
                    continue;
                }
                else {
                    //TODO: make dest dynamic depending on the enemy type
                    if(dist<=8) {
                        Direction dest=rc.getLocation().directionTo(ri.location).opposite();
                        if(rc.canMove(dest)) {
                            rc.move(dest);
                            return;
                        }
                    }
                }
            }
            if(hostile.length>0){
                if(rc.isWeaponReady() && rc.canAttackLocation(hostile[0].location)) {
                    rc.attackLocation(hostile[0].location);
                }
            }
        }
    }

    private static Direction randomDirection() {
        return Direction.values()[(int)(rnd.nextDouble()*8)];
    }
}
