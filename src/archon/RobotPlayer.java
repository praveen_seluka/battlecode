package archon;

import battlecode.common.*;

import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Pattern;

/**
 * Created by ps on 1/8/16.
 */
public class RobotPlayer {
    static Random rnd;

    static RobotController rc = null;
    static boolean isLeader = false;
    static int [] magneta = new int[]{238,0,238};
    static int scouts_created = 0;
    static int num_archons = 0;

    public static void run(RobotController xrc) {
        rc = xrc;
        rnd=new Random(rc.getID());
        Simp.setRandom(rnd);
        Simp.setRC(rc);
        scout_direction = randomDirection();

        leaderElection();


        while(true) {
            
            try {
                Memory.updateMemory();
            } catch (GameActionException e) {
                e.printStackTrace();
            }
            Message.load();

            if(rc.getRoundNum()==0)
                Clock.yield();
            try {
                if (rc.getType() == RobotType.ARCHON) {
                    archon();
                } else if (rc.getType() == RobotType.SOLDIER || rc.getType()==RobotType.GUARD) {
                    soldier();
                }
                else if(rc.getType() == RobotType.SCOUT) {
                    scout();
                }
            }catch (GameActionException e) {
                e.printStackTrace();
            }
            Clock.yield();
        }
    }

    static Direction scout_direction;
    private static void scout() throws GameActionException {
        //find parts, neutrals, enemies and send message
        //TODO: dont broadcast if enemy is nearby, save coreDelay to escape
        if(Memory.neutralMemory.size()>0){
            RobotInfo info=Memory.neutralMemory.remove();
            Message.sendMessage(info,MessageType.NEUTRAL,600);
        }
        else if(Memory.partMemory.size()>0){
            PartInfo info=Memory.partMemory.remove();
            Message.sendMessage(info,MessageType.PART,600);
        }
        else if(Memory.denMemory.size()>0){
            RobotInfo info=Memory.denMemory.remove();
            Message.sendMessage(info,MessageType.ZOMBIE_DEN,600);
        }


        boolean moved=false;
        if(rc.isCoreReady()) {
            if(rc.canMove(scout_direction) && Simp.isDirectionSafe(scout_direction)) {
                rc.move(scout_direction);
                moved=true;
            }
            else {
                Direction d =Simp.getSafestDirection();
                if(d!=null && rc.canMove(d)) {
                    rc.move(d);
                    scout_direction=d;
                    moved=true;
                }
            }
            if(!moved) {
                Direction to=Simp.getDirectionCanMove();
                if(to!=null) rc.move(to);
            }
        }
    }

    private static void soldier() throws GameActionException {
        MapLocation location=Memory.getDenLocation();
        attackSomeHostile();
        if(!hasTarget && location!=null) {
            //System.out.println("Got target "+location);
            target = location;
            hasTarget = true;
        }
        if(rc.isCoreReady() && hasTarget) {
            rc.broadcastSignal(rc.getType().sensorRadiusSquared);
            rc.setIndicatorString(0,""+target);
            double dist=rc.getLocation().distanceSquaredTo(target);

            if (dist < rc.getType().sensorRadiusSquared || rc.getLocation().equals(target)) {
                hasTarget = false;
            }
            Nav.navigate(target);
        }
        else{
            if(Message.soldierLocation!=null) {
                rc.setIndicatorString(0," soldier location target"+Message.soldierLocation);
                Direction d=rc.getLocation().directionTo(Message.soldierLocation);
                if(rc.isCoreReady() && rc.canMove(d))
                    rc.move(d);
            }
        }



        /*doOrders();
        if(rc.isCoreReady() && rc.canMove(dest)) {
            try {
                rc.move(dest);
            } catch (GameActionException e) {
                e.printStackTrace();
            }
        }*/
    }
    static MapLocation target=null;
    static boolean isTargetNeutral=false;
    static boolean hasTarget=false;
    public static void archon() throws GameActionException {
        Direction dest=randomDirection();
        repair();

        /*if(isLeader) {
            rc.broadcastSignal(100);
        }*/
        if(isEnemiesNearby()) {
            Direction to=Simp.getSafestDirection();
            if(to!=null && rc.isCoreReady())
                rc.move(to);
        }


        //TODO: dynamically spawn 1 scout per 100 round
        if(rc.getRoundNum()<25 && rc.isCoreReady()) {
            //int scouts_per_archon=(int)Math.ceil(4.0/num_archons);
            int scouts_per_archon = 1;
            if(scouts_created < scouts_per_archon) {
                if(rc.canBuild(dest,RobotType.SCOUT)) {
                    rc.build(dest,RobotType.SCOUT);
                    scouts_created++;
                }
            }
        }
        if(hasTarget) {

            if(rc.getLocation().equals(target)){
                hasTarget=false;
            }
            else if(rc.isCoreReady() && isTargetNeutral && rc.getLocation().isAdjacentTo(target)){
                //TODO: optimization to early detect target is not there
                hasTarget = false;
                isTargetNeutral = false;
                RobotInfo info=rc.senseRobotAtLocation(target);
                if(info!=null && info.team==Team.NEUTRAL) {
                    rc.activate(target);
                }
            }
            else if(rc.isCoreReady()){
                Nav.navigate(target);
            }
        }
        if (rc.isCoreReady() && rc.canBuild(dest, RobotType.SOLDIER)) {
            rc.build(dest, RobotType.SOLDIER);
        }

        MapLocation location=null;
        if(rc.isCoreReady() && (location=Memory.getNeutralLocation())!=null) {
            //System.out.println("Neutral Target "+location);
            target=location;hasTarget=true;isTargetNeutral=true;
            Nav.navigate(location);
        }

        if(rc.isCoreReady() && (location=Memory.getPartLocation())!=null) {
            //System.out.println("Part Target "+location);
            target=location;hasTarget=true;
            Nav.navigate(location);
        }
        if(hasTarget)
            rc.setIndicatorString(0,""+target);

        /*
        if(rnd.nextInt(100)<80) {
            attackSomeHostile();
        }*/
    }

    private static boolean isEnemiesNearby() {
        return (Memory.enemies.size()+Memory.zombies.size()) >0;
    }

    private static boolean isUnderAttack() {
        return (Memory.enemies.size()+Memory.zombies.size()) > Memory.friends.size();
    }
    private static void repair() throws GameActionException {
        RobotInfo []ours=rc.senseNearbyRobots(rc.getType().attackRadiusSquared, rc.getTeam());
        for(RobotInfo our:ours) {
            if(our.type!=RobotType.ARCHON && our.health < our.type.maxHealth) {
                rc.repair(our.location);
                return;
            }
        }
    }


    private static void doOrders() throws  GameActionException {
        if(rc.isCoreReady()) {
            Signal[] msgs = rc.emptySignalQueue();
            for (Signal msg : msgs) {
                if (msg.getTeam() == rc.getTeam()) {
                    Direction towards = rc.getLocation().directionTo(msg.getLocation());
                    if (rc.canMove(towards)) {
                        rc.move(towards);
                        return;
                    }
                }

            }
        }
    }
    private static void attackSomeHostile() throws GameActionException {

        if(rc.isCoreReady()){
            //TODO: outnumbered enemy? then attack
            //TODO: dont attack zombie dens, instead run away
            RobotInfo hostile[]=rc.senseHostileRobots(rc.getLocation(), rc.getType().sensorRadiusSquared);
            //TODO: pick the best direction to run away, getRunawayDirections
            for(RobotInfo ri:hostile){
                double dist=rc.getLocation().distanceSquaredTo(ri.location);

                if (ri.weaponDelay > 1){
                    continue;
                }
                else {
                    //TODO: make dest dynamic depending on the enemy type
                    if(dist<=8) {
                        Direction dest=rc.getLocation().directionTo(ri.location).opposite();
                        if(rc.canMove(dest)) {
                            rc.move(dest);
                            return;
                        }
                    }
                }
            }
            if(hostile.length>0){
                if(rc.isWeaponReady() && rc.canAttackLocation(hostile[0].location)) {
                    rc.attackLocation(hostile[0].location);
                }
            }
        }
    }

    private static void leaderElection() {
        if(rc.getType()==RobotType.ARCHON&&rc.getRoundNum()==0){
            int num_signals=0;
            while(rc.readSignal()!=null) num_signals++;
            num_archons = rc.getRobotCount();

            if(num_signals==0) {
                isLeader = true;
                //rc.setIndicatorDot(rc.getLocation().add(Direction.EAST,3),magneta[0],magneta[1],magneta[2]);
            }

            try {
                //TODO: check leader election, radius issue
                rc.broadcastSignal(1);
            } catch (GameActionException e) {
                e.printStackTrace();
            }

        }
    }

    private static Direction randomDirection() {
        return Direction.values()[(int)(rnd.nextDouble()*8)];
    }
}
