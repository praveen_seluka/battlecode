package archon;

import battlecode.common.*;

/**
 * Created by ps on 1/10/16.
 */
public class Message {
    static RobotController rc;
    static {
        rc=RobotPlayer.rc;
    }
    static MapLocation soldierLocation=null;
    public static void sendMessage(RobotInfo info, MessageType messageType, int radiusSquared) throws GameActionException {
        sendMessage(info.location,messageType,radiusSquared);
    }
    public static void sendMessage(PartInfo info, MessageType messageType, int radiusSquared) throws GameActionException {
        sendMessage(info.location,messageType,radiusSquared);
    }

    public static void sendMessage(MapLocation location, MessageType messageType, int radiusSquared) throws GameActionException {
        int x= encodeLocation(location.x, location.y);
        int y= encodeOthers(messageType);
        //System.out.println("Message sending "+ info.location.x+" "+info.location.y+" "+messageType);
        rc.broadcastMessageSignal(x, y, radiusSquared);
    }

    static int encodeOthers(MessageType messageType) throws GameActionException {
        int sender_type=rc.getType().ordinal();
        int a=sender_type << 28;
        int b= messageType.ordinal() << 24;
        //System.out.println("robot type is "+sender_type+ " "+a+" "+b);
        return (a | b);
    }

    static int encodeLocation(int x, int y) {
        return (x << 16 | y);
    }

    public static void load() {
        Signal msg = null;
        while ((msg = rc.readSignal()) != null) {
            if (msg.getTeam() == rc.getTeam()) {
                //check scout
                int message[] = msg.getMessage();
                if (message != null) {
                    MapLocation loc = parseLocation(message[0]);
                    MessageType messageType = parseMessageType(message[1]);
                    int sender_type = parseSenderType(message[1]);
                    //TODO: move this into Memory
                    if (Memory.seen.contains(message[0]))
                        continue;
                    //System.out.println("Message: "+ messageType+" Location: "+loc+" from "+RobotType.values()[sender_type]);

                    addMemory(messageType, loc);
                } else {//basic message
                    soldierLocation=msg.getLocation();
                }
            }
        }
    }
    static int str=0;
    private static void addMemory(MessageType messageType, MapLocation loc) {
        //rc.setIndicatorString(str++,loc+" "+messageType);
        if(messageType==MessageType.NEUTRAL)
            Memory.neutralLocations.add(loc);
        else if(messageType==MessageType.PART)
            Memory.partLocations.add(loc);
        else if(messageType==MessageType.ZOMBIE_DEN)
            Memory.denLocations.add(loc);

    }

    private static int parseSenderType(int i) {
        int x=i & 0xF0000000;
        x = x>>28;
        return x;
    }

    private static MessageType parseMessageType(int i){
        int x= i & 0x0F000000;
        x = x>>24;
        return MessageType.values()[x];
    }

    private static MapLocation parseLocation(int i) {
        int x=i & 0xFFFF0000;
        x = x >> 16;
        int y=i & 0x0000FFFF;

        return new MapLocation(x,y);
    }


}

