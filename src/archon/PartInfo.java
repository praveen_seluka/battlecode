package archon;
import battlecode.common.*;
/**
 * Created by ps on 1/10/16.
 */
public class PartInfo implements Comparable<PartInfo>{
    MapLocation location;
    double amount;
    int distance;
    int round;
    //TODO: better sorting order based on distance
    public PartInfo(MapLocation loc, double amount,int round){
        this.location=loc;
        this.amount=amount;
        this.distance=distance;
    }

    @Override
    public int compareTo(PartInfo that) {
        if(this.distance<that.distance)
            return -1;
        else if(this.distance>that.distance)
            return 1;
        return 0;
    }
}
