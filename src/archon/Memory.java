package archon;

import battlecode.common.*;
import scala.Array;

import java.util.*;

/**
 * Created by ps on 1/10/16.
 */
public class Memory {
    static Queue<PartInfo> partMemory;
    static Queue<RobotInfo> neutralMemory;
    static Queue<RobotInfo> denMemory;

    static HashSet<MapLocation> neutralLocations;
    static HashSet<MapLocation> partLocations;
    static HashSet<MapLocation> denLocations;
    static HashSet<Integer> seen;
    static RobotController rc;

    static {
        partMemory=new LinkedList<>();
        neutralMemory=new LinkedList<>();
        denMemory=new LinkedList<>();
        neutralLocations=new HashSet<MapLocation>();
        partLocations=new HashSet<MapLocation>();
        denLocations=new HashSet<>();

        rc = RobotPlayer.rc;
        seen = new HashSet<Integer>();
    }

    static void storePart(PartInfo x) {
        int hash=Message.encodeLocation(x.location.x, x.location.y);
        if(!seen.contains(hash)) {
            //System.out.println("Memory add part"+x.location);
            partMemory.add(x);
            seen.add(hash);
        }

    }
    //TODO: try sending message again ? remove from seen to recache it in memory
    static void storeNeutral(RobotInfo x){
        int hash=Message.encodeLocation(x.location.x, x.location.y);
        if(!seen.contains(hash)) {
            //System.out.println("Memory add neutral"+x.location);
            neutralMemory.add(x);
            seen.add(hash);
        }
    }

    static MapLocation getPartLocation() throws GameActionException {
        if(partMemory.size()>0) {
            return partMemory.remove().location;
        }
        else if(partLocations.size()>0){
            //find the nearest part location
            MapLocation location=Simp.findNearestInSet(partLocations);
            partLocations.remove(location);
            return location;
        }
        return null;
    }

    static MapLocation getNeutralLocation() throws GameActionException {
        //TODO: break archon ties and let one archon target
        if(neutralMemory.size()>0) {
            return neutralMemory.remove().location;
        }
        else if(neutralLocations.size()>0){
            //find the nearest part location
            MapLocation location=Simp.findNearestInSet(neutralLocations);
            neutralLocations.remove(location);
            return location;
        }
        return null;
    }

    static MapLocation getDenLocation() throws GameActionException {
        //TODO: break archon ties and let one archon target
        if(denMemory.size()>0) {
            return denMemory.remove().location;
        }
        else if(denLocations.size()>0){
            //find the nearest part location
            MapLocation location=Simp.findNearestInSet(denLocations);
            denLocations.remove(location);
            return location;
        }
        return null;
    }

    static ArrayList<MapLocation> corners=new ArrayList<>(4);

    static ArrayList<RobotInfo> enemies;
    static ArrayList<RobotInfo> friends;
    static ArrayList<RobotInfo> zombies;
    static void updateMemory() throws GameActionException {
        zombies=new ArrayList<>();
        friends=new ArrayList<>();
        enemies=new ArrayList<>();

        MapLocation[] ls = MapLocation.getAllMapLocationsWithinRadiusSq(rc.getLocation(), rc.getType().sensorRadiusSquared);
        RobotInfo info;
        for (MapLocation l : ls) {
            double amount=rc.senseParts(l);
            if(amount>0) {
                int dist=rc.getLocation().distanceSquaredTo(l);
                storePart(new PartInfo(l, amount, dist));
            }
            else if((info=rc.senseRobotAtLocation(l))!=null) {
                if(info.team==Team.NEUTRAL)
                    storeNeutral(info);
                else if(info.team==Team.ZOMBIE)
                    zombies.add(info);
                else if(info.team==rc.getTeam() && info.type.canAttack())
                    friends.add(info);
                else if(info.type.canAttack())
                    enemies.add(info);
                rc.setIndicatorString(0,"Enemies "+enemies.size()+" Friends "+friends.size()+" Zombies "+zombies.size());
                if(info.type==RobotType.ZOMBIEDEN)
                    storeDen(info);
            }
            //else if(!rc.onTheMap(l))
            //   numberOfTilesOutside++;
        }

    }

    private static void storeDen(RobotInfo x) {
        if(!denLocations.contains(x.location)) {
            //System.out.println("Memory add neutral"+x.location);
            denLocations.add(x.location);
            denMemory.add(x);
        }
    }

    static void updateMemoryOld() {
        for(PartInfo x:Simp.getParts(false)) {
            storePart(x);
        }

        for(RobotInfo info:rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, Team.NEUTRAL)) {
            storeNeutral(info);
        }
    }
    static void junk() {
         /*if(rc.getType()==RobotType.SCOUT && numberOfTilesOutside >3 && numberOfTilesOutside<15 && corners.size()<4) {
            boolean seenCorner=false;
            for(MapLocation c:corners) {
                if(rc.getLocation().distanceSquaredTo(c) < 500) {
                    seenCorner=true;
                    break;
                }
            }
            if(!seenCorner) {
                System.out.println("Checking for corners ");
                //MapLocation lsSmall[]=MapLocation.getAllMapLocationsWithinRadiusSq(rc.getLocation(), 18);
                MapLocation x = Simp.findCornerLocation(ls);
                if (x != null && !corners.contains(x)) {
                    corners.add(x);
                    System.out.println("Corner: " + x);
                    rc.setIndicatorDot(x, 238, 0, 238);
                }
            }

        }*/

    }

}
