package strat;

import battlecode.common.*;
import java.util.*;

/**
 * Created by ps on 1/8/16.
 */
public class RobotPlayer {
    static Random rnd;

    static RobotController rc = null;
    static boolean isLeader = false;
    static int [] magneta = new int[]{238,0,238};
    static int scouts_created = 0;
    static int num_archons = 0;

    static FastIterableSet allLocations[]=new FastIterableSet[4];
    static FastIntMap allBroadcast[]=new FastIntMap[4];
    static MessageType allType[]=new MessageType[]{MessageType.ZOMBIE_DEN,MessageType.NEUTRAL,MessageType.PART,MessageType.DEN_GONE};

    static FastIterableSet neutralLocations;
    static FastIterableSet neutralArchonLocations;
    static FastIterableSet partLocations;
    static FastIterableSet denLocations;
    static FastIterableSet densGone;
    static HashMap<Integer,MapLocation> enemyArchonLocations;
    static FastIntMap denBroadcast;
    static FastIntMap densGoneBroadcast;
    static FastIntMap neutralBroadcast;
    static FastIntMap partBroadcast;


    public static void run(RobotController xrc) {
        rc = xrc;
        rnd=new Random(rc.getID());
        Simp.setRandom(rnd);
        Simp.setRC(rc);
        scout_direction = randomDirection();
        soldierDirection=randomDirection();

        neutralLocations=new FastIterableSet();
        neutralArchonLocations=new FastIterableSet();
        partLocations=new FastIterableSet();
        denLocations=new FastIterableSet();
        densGone=new FastIterableSet();
        denBroadcast=new FastIntMap();
        densGoneBroadcast=new FastIntMap();
        neutralBroadcast=new FastIntMap();
        partBroadcast=new FastIntMap();
        enemyArchonLocations=new HashMap<>();

        allLocations[0]=denLocations;
        allLocations[1]=neutralLocations;
        allLocations[2]=partLocations;
        allLocations[3]=densGone;
        allBroadcast[0]=denBroadcast;
        allBroadcast[1]=neutralBroadcast;
        allBroadcast[2]=partBroadcast;
        allBroadcast[3]=densGoneBroadcast;

        leaderElection();
        opponent=Team.values()[(1-rc.getTeam().ordinal())];

        while(true) {

            if(rc.getRoundNum()==0)
                Clock.yield();
            try {
                Message.load();
                //enemyArchonLocations.clear();
                hostileRobots=rc.senseHostileRobots(rc.getLocation(), rc.getType().sensorRadiusSquared);
                zombieRobots=rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, Team.ZOMBIE);
                enemyRobots=rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, opponent);
                RobotInfo myRobots[]=rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, rc.getTeam());
                ourRobots=new RobotInfo[myRobots.length+1];
                for(int i=0;i<myRobots.length;i++)
                    ourRobots[i]=myRobots[i];
                ourRobots[myRobots.length]=rc.senseRobotAtLocation(rc.getLocation());

                if (rc.getType() == RobotType.ARCHON) {
                    archon();
                } else if (rc.getType() == RobotType.SOLDIER || rc.getType()==RobotType.GUARD || rc.getType()==RobotType.VIPER || rc.getType()==RobotType.TURRET) {
                    soldier();
                }
                else if(rc.getType() == RobotType.SCOUT) {
                    scout();
                }
            }catch (GameActionException e) {
                e.printStackTrace();
            }
            Clock.yield();
        }
    }

    static Direction scout_direction;
    static int lastArchonSeenRound=0;
    static int enemyArchonIndex=0;
    private static void scout() throws GameActionException {
        int numAttackable=0;
        for(RobotInfo info:hostileRobots){
            if(info.type!=RobotType.ARCHON && info.type!=RobotType.SCOUT && info.type!=RobotType.ZOMBIEDEN)
                numAttackable++;
        }

        rc.setIndicatorString(0,numAttackable+"");

        if(rc.isCoreReady() && hostileRobots.length>0) {

            rc.setIndicatorString(0,numAttackable+" "+rc.getRoundNum());
            if(Simp.isDirectionSafe(scout_direction)){
                if(rc.canMove(scout_direction))
                rc.move(scout_direction);
            }
            else {
                Direction to = Simp.getSafestDirection2();
                if (to != null) {
                    rc.move(to);
                    scout_direction = to;
                    return;
                }
            }
        }
        checkAllLocations();

        for(RobotInfo info:rc.senseNearbyRobots()){
            if(info.type==RobotType.ARCHON) {
                if(info.team==opponent) {
                    Message.sendMessage(info.location, MessageType.ENEMYARCHON, info.ID, 600);
                    enemyArchonLocations.put(info.ID,info.location);
                }
                else if(info.team==Team.NEUTRAL)
                    Message.sendMessage(info.location, MessageType.NEUTRALARCHON, 600);
            }
            else if(info.team==Team.NEUTRAL)
                neutralLocations.add(info.location);
            else if(info.type==RobotType.ZOMBIEDEN)
                denLocations.add(info.location);
            else if(info.type==RobotType.ARCHON && info.team==rc.getTeam()) {
                lastArchonSeenRound=rc.getRoundNum();
            }
        }

        MapLocation lastLocation=null;
        for(MapLocation part:rc.sensePartLocations(rc.getType().sensorRadiusSquared)) {
            if(lastLocation==null) {
                lastLocation = part;
                partLocations.add(part);
            }
            else if(lastLocation!=null){
                int distance=part.distanceSquaredTo(lastLocation);
                if(distance<20)
                    continue;
                else {
                    lastLocation=part;
                    partLocations.add(part);
                }
            }
        }


        if(numAttackable==0) {
            rc.setIndicatorString(1," None attacking,broadcasting at "+rc.getRoundNum());
            if(rc.getRoundNum()>500){
                int cur=0;
                for(Integer id:enemyArchonLocations.keySet()){
                    if(cur==(enemyArchonIndex%enemyArchonLocations.size())) {
                        MapLocation loc = enemyArchonLocations.get(id);
                        rc.setIndicatorString(1," None attacking, enemyArchon ID "+id+" loc "+loc);
                        Message.sendMessage(loc, MessageType.ENEMYARCHON, id, 600);
                        break;
                    }
                    cur++;
                    enemyArchonIndex++;
                }
            }
            broadcastNewOnes();
        }

        boolean moved=false;
        if(rc.isCoreReady()) {
            rc.setIndicatorString(0, "" + rc.getRoundNum() + " " + scout_direction);
            if(rc.canMove(scout_direction)) {
                rc.move(scout_direction);
                moved=true;
            }
            if(!moved) {
                Direction to=Simp.getDirectionCanMove();
                if(to!=null) {
                    rc.move(to);
                    scout_direction=to;
                }
            }
        }
    }

    private static void checkAllLocations() throws GameActionException {
        for(MapLocation loc:denLocations.getKeys()){
            if(rc.canSense(loc)){
                RobotInfo info=rc.senseRobotAtLocation(loc);
                if(info==null || info.type!=RobotType.ZOMBIEDEN) {
                    denLocations.remove(loc);
                    densGone.add(loc);
                }
            }
        }
        for(MapLocation loc:neutralLocations.getKeys()){
            if(rc.canSense(loc)){
                RobotInfo info=rc.senseRobotAtLocation(loc);
                if(info==null || info.team!=Team.NEUTRAL)
                    neutralLocations.remove(loc);
            }
        }
        for(MapLocation loc:partLocations.getKeys()){
            if(rc.canSense(loc)){
                double info=rc.senseParts(loc);
                if(info==0.0)
                    partLocations.remove(loc);
            }
        }
    }

    private static boolean broadcastNewOnes() throws GameActionException {
        FastIterableSet locations;
        FastIntMap sentBroadcast;
        MessageType type;
        int radius=600;
        MapLocation sentEarlier[]=new MapLocation[4];
        for(int k=0;k<4;k++) {
            locations=allLocations[k];
            sentBroadcast=allBroadcast[k];
            type=allType[k];

            int earliest_round=3000;
            MapLocation location=null;
            if (locations.getSize() == 0)
                continue;
            for (MapLocation loc : locations.getKeys()) {
                if (sentBroadcast.contains(loc)) {
                    int lastSentRound=sentBroadcast.get(loc);
                    if(lastSentRound<earliest_round){
                        earliest_round=lastSentRound;
                        location=loc;
                    }
                } else {
                    Message.sendMessage(loc, type, radius);
                    sentBroadcast.add(loc, rc.getRoundNum());
                    rc.setIndicatorString(2, " round: " + rc.getRoundNum() + " New, Type: "+type +" Location: "+ loc);
                    return true;
                }
            }
            if(location!=null && earliest_round<(rc.getRoundNum()-100)) {
                sentEarlier[k]=location;
            }
        }

        for(int k=0;k<4;k++){
            type=allType[k];
            if(sentEarlier[k]!=null){
                Message.sendMessage(sentEarlier[k],type,radius);
                allBroadcast[k].add(sentEarlier[k], rc.getRoundNum());
                rc.setIndicatorString(2, " round: " + rc.getRoundNum() + " Old, Type: " +type +" Location: "+ sentEarlier[k]);
            }
        }
        return false;
    }

    private static boolean broadcast(FastIterableSet locations, FastIntMap sentBroadcast,MessageType type, int radius) throws GameActionException {
        int earliest_round=3000;
        MapLocation location=null;
        if(locations.getSize()==0)
            return false;
        for(MapLocation loc:locations.getKeys()){
            if(sentBroadcast.contains(loc)){
                int lastSentRound=sentBroadcast.get(loc);
                if(lastSentRound<earliest_round){
                    earliest_round=lastSentRound;
                    location=loc;
                }
            }
            else{
                Message.sendMessage(loc, type, radius);
                sentBroadcast.add(loc, rc.getRoundNum());
                rc.setIndicatorString(2," round "+rc.getRoundNum()+" first time "+loc);
                return true;
            }
        }
        if(location!=null && earliest_round<(rc.getRoundNum()-100)) {
            Message.sendMessage(location, type, radius);
            sentBroadcast.add(location,rc.getRoundNum());
            rc.setIndicatorString(2," round "+rc.getRoundNum()+" not first time"+location);

            return true;
        }
        return false;
    }



    private static MapLocation findNearestLocation(MapLocation[] locations,FastLocSet seen) {
        double minDistance=Double.MAX_VALUE;
        if(locations.length==0)
            return null;
        MapLocation location=null;
        for(MapLocation den:locations){
            if(seen!=null && seen.contains(den))
                continue;
            double dist=rc.getLocation().distanceSquaredTo(den);
            if(dist<minDistance){
                minDistance=dist;
                location=den;
            }
        }
        return location;
    }

    static int enemyArchonId=-1;
    private static MapLocation findNearestArchon(HashMap<Integer,MapLocation> eArchons,FastLocSet seenEnemyArchonLocs) {
        double minDistance=Double.MAX_VALUE;
        if(eArchons.size()==0)
            return null;
        MapLocation location=null;
        for(Integer id:eArchons.keySet()){
            MapLocation archon=eArchons.get(id);
            if(seenEnemyArchonLocs!=null && seenEnemyArchonLocs.contains(archon))
                continue;
            double dist=rc.getLocation().distanceSquaredTo(archon);
            if(dist<minDistance){
                minDistance=dist;
                location=archon;
                enemyArchonId=id;
            }
        }
        return location;
    }

    static RobotInfo zombieRobots[]=null;
    static RobotInfo enemyRobots[]=null;
    static RobotInfo ourRobots[]=null;
    static RobotInfo hostileRobots[]=null;
    static Team opponent;
    static FastLocSet seenDens=new FastLocSet();
    static FastLocSet seenEnemyArchonLocs=new FastLocSet();
    static Direction soldierDirection=null;
    static boolean isTargetEnemyArchon=false;
    private static void soldier() throws GameActionException {

        if(zombieRobots.length>0 && enemyRobots.length==0) {
            if(attackOrRetreatZombie())
                return;
        }
        else if(enemyRobots.length>0){
            if(attackOrRetreat2())
                return;
        }

        for(RobotInfo info:zombieRobots){
            if(info.type==RobotType.ZOMBIEDEN){
                if(rc.isWeaponReady() && rc.canAttackLocation(info.location))
                    rc.attackLocation(info.location);
                else if(rc.isCoreReady())
                    Nav.navigate(info.location);
            }
        }
        if(!hasTarget){
            if(Message.helpLocation!=null) {
                rc.setIndicatorString(1, " Round "+rc.getRoundNum()+" soldier location target " + Message.helpLocation);
                double dist=rc.getLocation().distanceSquaredTo(Message.helpLocation);

                 if(moveNearThis(Message.helpLocation))
                    return;
            }
        }
        if(!hasTarget) {
            MapLocation location = findNearestLocation(denLocations.getKeys(), seenDens);
            rc.setIndicatorString(0, " Round " + rc.getRoundNum()+" den " + location);

            if (location == null) {
                location = findNearestArchon(enemyArchonLocations,seenEnemyArchonLocs);
                if(location!=null)
                    isTargetEnemyArchon=true;

                rc.setIndicatorString(0, " Round "+ rc.getRoundNum()+" enemyArchon "+ location);

            }

            if(location!=null) {
                //System.out.println("Got target "+location);
                target = location;
                hasTarget = true;
                if(isTargetEnemyArchon){
                    enemyArchonLocations.remove(enemyArchonId);
                }
                denLocations.remove(location);
                seenDens.add(location);
            }
        }

        if(rc.isCoreReady() && hasTarget) {
            double dist=rc.getLocation().distanceSquaredTo(target);

            if (dist < rc.getType().sensorRadiusSquared || rc.getLocation().equals(target)) {
                hasTarget = false;
            }
            else {
                //rc.broadcastSignal(rc.getType().sensorRadiusSquared*3);
                rc.setIndicatorString(0,""+target);
            }
            Nav.navigate(target);
        }
        else{
            if(true) {
                if(rc.isCoreReady() && rc.canMove(soldierDirection) && rc.senseRubble(rc.getLocation().add(soldierDirection))==0) {
                    rc.move(soldierDirection);
                    return;
                }
                else {
                    Direction to=Simp.getDirectionCanMove();
                    if(rc.isCoreReady() && to!=null) {
                        rc.move(to);
                        soldierDirection=to;
                        return;
                    }
                }
            }
            else{
                int sumX = 0;
                int sumY = 0;
                int aveX = 0;
                int aveY = 0;
                for (int i=0;i<ourRobots.length;i++) {
                    RobotInfo friend=ourRobots[i];
                    sumX += friend.location.x;
                    sumY += friend.location.y;
                }
                if (ourRobots.length != 0) {
                    aveX = sumX / ourRobots.length;
                    aveY = sumY / ourRobots.length;
                    if (moveNearThis(new MapLocation(aveX, aveY)))
                        return;
                }
            }
            if(rc.isCoreReady()){
                for(Direction d:moveableDirections){
                    if(rc.isCoreReady() && rc.getType()!=RobotType.TURRET && rc.senseRubble(rc.getLocation().add(d))>0)
                        rc.clearRubble(d);
                }
            }
        }
    }

    static Direction moveableDirections[]=new Direction[]{
            Direction.EAST,Direction.WEST,
            Direction.NORTH,Direction.NORTH_EAST,
            Direction.SOUTH,Direction.SOUTH_EAST,
            Direction.NORTH_WEST,Direction.SOUTH_WEST};



    private static boolean attackOrRetreat2() throws GameActionException {
        int numUnderAttack=0;
        int numL1=0;
        int numL2=0;
        int numL3=0;
        int thisLevel=0;

        for(int i=ourRobots.length-1;i>0;i--){
            if(ourRobots[i].type.attackPower==0.0)
                continue;
            int minLevel=10;
            for(int j=0;j<hostileRobots.length;j++){
                int dist=ourRobots[i].location.distanceSquaredTo(hostileRobots[j].location);
                int attackRadiusSquared=hostileRobots[j].type.attackRadiusSquared;

                int l1Radius=getl1radius(hostileRobots[j].type);
                int l2Radius=getl2radius(hostileRobots[j].type);

                int level=10;
                if(dist<=attackRadiusSquared)
                    level=0;
                else if(dist<=l1Radius)
                    level=1;
                else if(dist<=l2Radius)
                    level=2;
                else
                    level=3;
                if(level<minLevel)
                    minLevel=level;
            }
            if(i==ourRobots.length-1){
                thisLevel=minLevel;
                if(minLevel!=1)
                    break;
            }
            if(minLevel==0) numUnderAttack++;
            else if(minLevel==1) numL1++;
            else if(minLevel==2) numL2++;
            else numL3++;

        }

        rc.setIndicatorString(0,thisLevel+" "+numUnderAttack+" "+numL1+" "+numL2+" "+numL3+" ");

        if(thisLevel==0){
            if(tryRunningAway(thisLevel))
                return true;
            else if(runSafestDirection2())
                return true;
            else if(attackAnyPossible(hostileRobots))
                return true;
            else {
                rc.broadcastSignal(rc.getType().sensorRadiusSquared * 4);
                return true;
            }
        }
        if(thisLevel==1 && numL1>hostileRobots.length){
            rc.broadcastSignal(rc.getType().sensorRadiusSquared*2);
            if(attackAnyPossible(hostileRobots))
                return true;
            else {
                Direction center = directionTowardsCenter(hostileRobots);
                if (center != null && rc.isCoreReady() && rc.canMove(center)) {
                    rc.move(center);
                    return true;
                }
            }
        }
        //We are stronger
        if(thisLevel==1 && (numL1+numL2)>hostileRobots.length){
            rc.broadcastSignal(rc.getType().sensorRadiusSquared*2);
            if(attackAnyPossible(hostileRobots))
                return true;
            else
                return true;//stay ground
        }

        if(thisLevel==1 && (numL1+numL2)<=hostileRobots.length){
            rc.broadcastSignal(rc.getType().sensorRadiusSquared*2);
            if(tryRunningAway(thisLevel))
                return true;
            else if(runSafestDirection2())
                return true;
            else if(attackAnyPossible(hostileRobots))
                return true;
            else {
                rc.broadcastSignal(rc.getType().sensorRadiusSquared * 4);
                return true;
            }

        }
        else if(thisLevel>=2){
            rc.broadcastSignal(rc.getType().sensorRadiusSquared * thisLevel * 4);

            if(attackAnyPossible(hostileRobots))
                return true;
            //move closer
            if(hostileRobots.length>0) {
                Direction center = directionTowardsCenter(hostileRobots);
                if (center != null && rc.isCoreReady() && rc.canMove(center)) {
                    rc.move(center);
                    return true;
                }
            }

        }
        return false;
    }

    private static boolean attackOrRetreatZombie() throws GameActionException {
        int numUnderAttack=0;
        int numL1=0;
        int numL2=0;
        int numL3=0;
        int thisLevel=0;

        for(int i=ourRobots.length-1;i>0;i--){
            if(ourRobots[i].type.attackPower==0.0)
                continue;
            int minLevel=10;
            for(int j=0;j<zombieRobots.length;j++){
                int dist=ourRobots[i].location.distanceSquaredTo(zombieRobots[j].location);
                int attackRadiusSquared=zombieRobots[j].type.attackRadiusSquared;

                int l1Radius=getl1radius(zombieRobots[j].type);
                int l2Radius=getl2radius(zombieRobots[j].type);

                int level=10;
                if(dist<=attackRadiusSquared)
                    level=0;
                else if(dist<=l1Radius)
                    level=1;
                else if(dist<=l2Radius)
                    level=2;
                else
                    level=3;
                if(level<minLevel)
                    minLevel=level;
            }
            if(i==ourRobots.length-1){
                thisLevel=minLevel;
                if(minLevel!=1)
                    break;
            }
            if(minLevel==0) numUnderAttack++;
            else if(minLevel==1) numL1++;
            else if(minLevel==2) numL2++;
            else numL3++;

        }

        rc.setIndicatorString(0,thisLevel+" "+numUnderAttack+" "+numL1+" "+numL2+" "+numL3+" ");

        if(thisLevel==0 || thisLevel==1){
            if(tryRunningAway(thisLevel))
                return true;
            else if(runSafestDirection2())
                return true;
            else if(attackAnyPossibleZombie(zombieRobots))
                return true;
            else {
                rc.broadcastSignal(rc.getType().sensorRadiusSquared * 4);
                return true;
            }
        }
        else if(thisLevel==2){
            rc.broadcastSignal(rc.getType().sensorRadiusSquared*2);
            if(attackAnyPossibleZombie(zombieRobots))
                return true;
            else
                return true;//stay ground
        }
        else if(thisLevel>=3){
            rc.broadcastSignal(rc.getType().sensorRadiusSquared * thisLevel * 4);

            if(attackAnyPossibleZombie(zombieRobots))
                return true;
            //move closer
            if(zombieRobots.length>0) {
                Direction center = directionTowardsCenter(hostileRobots);
                if (center != null && rc.isCoreReady() && rc.canMove(center)) {
                    rc.move(center);
                    return true;
                }
            }

        }
        /*if(thisLevel==1 && numL1>zombieRobots.length){
            rc.broadcastSignal(rc.getType().sensorRadiusSquared*2);
            if(attackAnyPossibleZombie(zombieRobots))
                return true;
            else {
                Direction center = directionTowardsCenter(zombieRobots);
                if (center != null && rc.isCoreReady() && rc.canMove(center)) {
                    rc.move(center);
                    return true;
                }
            }
        }
        //We are stronger
        if(thisLevel==1 && (numL1+numL2)>zombieRobots.length){
            rc.broadcastSignal(rc.getType().sensorRadiusSquared*2);
            if(attackAnyPossibleZombie(zombieRobots))
                return true;
            else
                return true;//stay ground
        }

        if(thisLevel==1 && (numL1+numL2)<=zombieRobots.length){
            rc.broadcastSignal(rc.getType().sensorRadiusSquared * 2);
            if(tryRunningAway(thisLevel))
                return true;
            else if(runSafestDirection2())
                return true;
            else if(attackAnyPossibleZombie(zombieRobots))
                return true;
            else {
                rc.broadcastSignal(rc.getType().sensorRadiusSquared * 4);
                return true;
            }

        }
        else if(thisLevel>=2){
            rc.broadcastSignal(rc.getType().sensorRadiusSquared * thisLevel * 4);

            if(attackAnyPossibleZombie(zombieRobots))
                return true;
            //move closer
            if(zombieRobots.length>0) {
                Direction center = directionTowardsCenter(hostileRobots);
                if (center != null && rc.isCoreReady() && rc.canMove(center)) {
                    rc.move(center);
                    return true;
                }
            }

        }*/
        return false;
    }

    static boolean runSafestDirection2() throws GameActionException {
        //RobotInfo [] hostiles=rc.senseHostileRobots(rc.getLocation(),rc.getType().sensorRadiusSquared);
        RobotInfo[] hostiles = hostileRobots;
        Direction[] goods = new Direction[8];
        int aveX = 0;
        int aveY = 0;
        int numberOfHostiles = hostiles.length;
        for (RobotInfo hostile : hostiles) {
            if (hostile.type == RobotType.FASTZOMBIE) {
                aveX += hostile.location.x;
                aveY += hostile.location.y;
                numberOfHostiles++;
            }

            aveX += hostile.location.x;
            aveY += hostile.location.y;
        }
        int x = aveX / numberOfHostiles;
        int y = aveY / numberOfHostiles;
        MapLocation center = new MapLocation(x, y);
        Direction to = rc.getLocation().directionTo(center).opposite();
        rc.setIndicatorString(1, "enemy center " + center.toString());
        if (rc.isCoreReady() && rc.canMove(to)){
            rc.move(to);
            return true;
        }

        else {
            if (rc.isCoreReady() && rc.canMove(to.rotateLeft())) {
                rc.move(to.rotateLeft());
                return true;
            }
            else if (rc.isCoreReady() && rc.canMove(to.rotateRight())) {
                rc.move(to.rotateRight());
                return true;
            }
        }
        return false;
    }
    private static boolean tryRunningAway(int level) throws GameActionException {
        if(!rc.isCoreReady())
            return false;
        Direction goodButRubble[]=new Direction[8];
        int index=0;
        for(Direction d:moveableDirections){
            boolean good=true;
            MapLocation newlocation=rc.getLocation().add(d);
            for(int i=0;i<hostileRobots.length;i++){
                int distance=newlocation.distanceSquaredTo(hostileRobots[i].location);

                int l1Radius=getl1radius(hostileRobots[i].type);
                if(level==1 && distance<=l1Radius){
                    good=false;
                    break;
                }
                else if(level==0 && distance<=hostileRobots[i].type.attackRadiusSquared){
                    good=false;
                    break;
                }

            }
            if(good){
                if(rc.senseRubble(newlocation)>0){
                    goodButRubble[index++]=d;
                }
                else if(rc.isCoreReady()&& rc.canMove(d)) {
                    rc.move(d);
                    return true;
                }
            }
        }
        if(index>0) {
            if (rc.isCoreReady() && rc.canMove(goodButRubble[0])){
                rc.move(goodButRubble[0]);
                return true;
            }
        }

        return false;
    }

    private static int getl2radius(RobotType type) {
        switch (type.ordinal()){
            case 0:
                return 8;
            case 1:
                return 18;
            case 2:
                return 41;
            case 3:
                return 18;
            case 4:
                return 18;
            case 5:
                return 8;
            case 6:
                return 18;
            case 7:
                return 41;
            case 8:
                return 18;
            case 9:
                return 50;
            case 10:
                return 50;
            case 11:
                return 50;
            default:
                return 18;
        }
    }

    private static int getl1radius(RobotType type) {
        switch (type.ordinal()){
            case 0:
                return 2;
            case 1:
                return 8;
            case 2:
                return 25;
            case 3:
                return 8;
            case 4:
                return 8;
            case 5:
                return 2;
            case 6:
                return 8;
            case 7:
                return 25;
            case 8:
                return 8;
            case 9:
                return 32;
            case 10:
                return 32;
            case 11:
                return 32;
            default:
                return 8;
        }

    }

    private static boolean attackAnyPossible(RobotInfo[] robots) throws GameActionException {
        if(!rc.isWeaponReady())
            return false;
        double weakest=Double.MAX_VALUE;
        MapLocation weakestLocation=null;
        for(int i=0;i<robots.length;i++){
            RobotInfo robot=robots[i];
            if(rc.isWeaponReady() && rc.canAttackLocation(robot.location)){
                if(robot.health<weakest)
                {
                    weakest=robot.health;
                    weakestLocation=robot.location;
                }
                //rc.attackLocation(robot.location)
            }
        }
        if(weakestLocation!=null) {
            rc.attackLocation(weakestLocation);
            return true;
        }
        return false;
    }

    private static boolean attackAnyPossibleZombie(RobotInfo[] robots) throws GameActionException {
        if(!rc.isWeaponReady())
            return false;
        //DEN,STANDARDZOMBIE, RANGEDZOMBIE, FASTZOMBIE, BIGZOMBIE,
        RobotInfo z[][]=new RobotInfo[5][robots.length];
        int index[]=new int[5];
        for(int i=0;i<robots.length;i++){
            int roll=robots[i].type.ordinal();
            int loc=index[roll];
            z[roll][loc]=robots[i];
            index[roll]++;
        }
        int order[]=new int[]{4,2,3,1,0};
        for(int x=0;x<5;x++) {
            int type=order[x];
            double weakest=Double.MAX_VALUE;
            MapLocation weakestLocation=null;
            for (int i = 0; i < index[type]; i++) {
                RobotInfo robot=z[type][i];
                if (rc.canAttackLocation(robot.location)) {
                    if(robot.health<weakest)
                    {
                        weakest=robot.health;
                        weakestLocation=robot.location;
                    }
                }
            }
            if(weakestLocation!=null) {
                rc.attackLocation(weakestLocation);
                return true;
            }
        }

        return false;
    }

    private static RobotInfo[] top3WeakestRangedZombie(RobotInfo robots[]) {
        double health[]=new double[robots.length];
        for(int i=0;i<robots.length;i++)
            health[i]=robots[i].health;
        for(int i=0;i<robots.length;i++){
            int minHealthIndex=i;
            double minHealth=health[i];
            for(int j=i+1;j<robots.length;j++){
                if(health[j]<minHealth){
                    minHealth=health[j];
                    minHealthIndex=j;
                }
            }
            double temp=health[i];RobotInfo temp1=robots[i];
            health[i]=health[minHealthIndex];robots[i]=robots[minHealthIndex];
            health[minHealthIndex]=temp;robots[minHealthIndex]=temp1;
        }
        return robots;
    }

    private static Direction directionTowardsCenter(RobotInfo[] robots) {
        Direction []goods=new Direction[8];
        int sumX=0;
        int sumY=0;
        for(int i=0;i<robots.length;i++){
            RobotInfo info=robots[i];
            sumX+=info.location.x;
            sumY+=info.location.y;
        }
        int aveX=sumX/robots.length;
        int aveY=sumY/robots.length;
        MapLocation center=new MapLocation(aveX,aveY);
        Direction to=rc.getLocation().directionTo(center);
        //rc.setIndicatorString(0,"enemy center "+center.toString());
        if(rc.canMove(to))
            return to;
        else {
            if(rc.canMove(to.rotateLeft()))
                return to.rotateLeft();
            else if(rc.canMove(to.rotateRight()))
                return to.rotateRight();
        }
        return null;
    }

    private static RobotInfo[] findZombiesByType(RobotInfo zombies[], RobotType type) {
        RobotInfo matches[]=new RobotInfo[zombies.length];
        int index=0;
        for(RobotInfo zombie:zombies) {
            if(zombie.type==type)
                matches[index++]=zombie;
        }
        RobotInfo m[]=new RobotInfo[index];
        for(int i=0;i<index;i++){
            m[i]=matches[i];
        }
        return m;
    }
    /*
     /*
        RobotInfo map[][]=new RobotInfo[(t*2)+1][(t*2)+1];
        int myX=rc.getLocation().x;
        int myY=rc.getLocation().y;
        for(RobotInfo hostile:hostileRobots){
            int x=hostile.location.x-myX;
            int y=hostile.location.y-myY;
            bfs(x,y,map);
        }

    int dx[]=new int[]{-1,0,1,1,1,0,-1,-1};
    int dy[]=new int[]{1,1,1,0,-1,-1,-1,0}
    private static void bfs(int x, int y, int[][] map) {
        boolean seen[][]=new boolean[map.length][map.length];
        Queue<MapLocation> queue=new LinkedList<>();
        queue.add(new MapLocation(x,y));
        map[x][y]=0;
        seen[x][y]=true;
        while(!queue.isEmpty()){
            MapLocation cur=queue.remove();
            for(int i=0;i<8;i++) {
                int newx=cur.x+dx[i];
                int newy=cur.y+dy[i];
                if(seen[newx][newy])
                    continue;
                else{
                    map[newx][new]
                }
            }
        }
    }
    */

    private static boolean moveNearThis(MapLocation location) throws GameActionException {
        Direction d=rc.getLocation().directionTo(location);
        if(rc.isCoreReady() && rc.canMove(d)) {
            rc.move(d);
            return true;
        }
        Direction left=d.rotateLeft();
        Direction right=d.rotateRight();
        if(rc.isCoreReady() && rc.canMove(left)) {
            rc.move(left);
            return true;
        }
        else if(rc.isCoreReady() && rc.canMove(right)) {
            rc.move(right);
            return true;
        }
        //if(rc.senseRubble(location)>0){
        //    rc.clearRubble(d);
        //}

        return false;
    }

    private static boolean runAwayIfEnemyAdjacent(RobotInfo[] hostiles) throws GameActionException {
        int num_adjacent=0;
        int sumX=0;
        int sumY=0;
        for(RobotInfo hostile:hostiles) {
            if(hostile.type==RobotType.ZOMBIEDEN)
                continue;
            if(rc.getLocation().distanceSquaredTo(hostile.location) <=2) {
                num_adjacent++;
                sumX=sumX+hostile.location.x;
                sumY=sumY+hostile.location.y;
            }
        }
        if(num_adjacent==0)
            return false;
        int aveX=sumX/num_adjacent;
        int aveY=sumY/num_adjacent;
        Direction to=rc.getLocation().directionTo(new MapLocation(aveX,aveY)).opposite();
        if(rc.isCoreReady() && rc.canMove(to)){
            rc.move(to);
            return true;
        }
        return false;
    }

    private static boolean enemyAdjacent(RobotInfo[] hostiles) {
        for(RobotInfo hostile:hostiles) {
            if(rc.getLocation().distanceSquaredTo(hostile.location) <=2) {
                return true;
            }
        }
        return false;
    }

    private static MapLocation weakestLocation(RobotInfo[] hostiles) throws GameActionException {
        double health=Double.MAX_VALUE;
        RobotInfo target=null;
        for(RobotInfo hostile:hostiles) {
            if(hostile.health<health) {
                health=hostile.health;
                target=hostile;
            }
        }
        if(target!=null)
            return target.location;
        else
            return null;
    }

    static MapLocation target=null;
    static boolean isTargetNeutral=false;
    static boolean isTargetPart=false;
    static boolean isTargetNArchon=false;
    static boolean hasTarget=false;
    static int roundWhenScoutCreated=0;
    static FastLocSet seenNeutrals=new FastLocSet();
    static FastLocSet seenNeutralArchons=new FastLocSet();
    static FastLocSet seenParts=new FastLocSet();
    static int soldierMax=0;
    static int soldierForScout=0;
    static boolean buildViper=false;
    static boolean checkRobotCount=false;
    public static void archon() throws GameActionException {
        Direction dest=randomDirection();
        //if(rc.getRobotCount()>40)
        //    checkRobotCount=true;
        repair();
        if(hostileRobots.length>0) {
            int numAttackable=0;
            for(RobotInfo info:hostileRobots){
                if(info.type!=RobotType.ARCHON && info.type!=RobotType.SCOUT && info.type!=RobotType.ZOMBIEDEN)
                    numAttackable++;
            }
            if(numAttackable>0) {

                Direction to = Simp.getSafestDirection2();
                if(to==null)
                    rc.broadcastSignal(rc.getType().sensorRadiusSquared*6);
                else if(to != null && rc.isCoreReady()) {
                    rc.move(to);
                    return;
                }
            }
        }

        RobotType type=RobotType.SOLDIER;
        if(isLeader) {
            if (rc.getRoundNum() <= 1 || (rc.getRobotCount() % 50 == 0 && roundWhenScoutCreated < (rc.getRoundNum() - 100))) {
                type = RobotType.SCOUT;
                roundWhenScoutCreated = rc.getRoundNum();
            }
        }

        if(soldierMax>=10){
            if(!checkRobotCount)
                type= RobotType.VIPER;
            if(checkRobotCount && rc.getRobotCount()>40)
                type=RobotType.VIPER;
        }
        if(soldierForScout>=15){
            type=RobotType.SCOUT;
        }

        if (rc.isCoreReady()) {
            for(Direction d:moveableDirections) {
                if(rc.canBuild(d,type)) {
                    if(soldierMax>=10)
                        soldierMax = 0;
                    if(soldierForScout>=15)
                        soldierForScout=0;
                    if(type== RobotType.SOLDIER) {
                        soldierMax++;
                        soldierForScout++;
                    }
                    rc.build(d,type);
                    return;
                }
            }
        }

        rc.setIndicatorString(0, " Narchon size "+neutralArchonLocations.getSize()+" Neutral size" + neutralLocations.getSize() + " Part size " + partLocations.getSize());

        if(hasTarget) {

            rc.setIndicatorString(2,rc.getRoundNum()+" "+target);

            int distance=rc.getLocation().distanceSquaredTo(target);
            boolean targetExists=true;
            if(rc.canSense(target)) {
                if (isTargetNeutral || isTargetNArchon) {
                    RobotInfo info = rc.senseRobotAtLocation(target);
                    if (info == null || info.team != Team.NEUTRAL) {
                        targetExists = false;
                        hasTarget=false;
                    }

                } else {
                    if (rc.senseParts(target) == 0.0) {
                        targetExists = false;
                        hasTarget=false;
                    }
                    else {
                        RobotInfo info=rc.senseRobotAtLocation(target);
                        if(info!=null && info.type==RobotType.TURRET){
                            hasTarget=false;
                            targetExists=false;
                        }
                    }
                }
            }
            if(!targetExists ||  distance>rc.getType().sensorRadiusSquared){
                MapLocation location=null;
                boolean foundNeutral=false;

                for(RobotInfo info:rc.senseNearbyRobots()){
                    if(seenNeutrals.contains(info.location))
                        continue;
                    if(info.team==Team.NEUTRAL) {
                        location=info.location;
                        break;
                    }
                }
                if(location!=null) {
                    if(targetExists && isTargetNeutral && target!=null){
                        neutralLocations.add(target);
                        seenNeutrals.remove(target);
                    }
                    else if(targetExists && isTargetPart && target!=null){
                        partLocations.add(target);
                        seenParts.remove(target);
                    }
                    else if(targetExists && isTargetNArchon && target!=null){
                        neutralArchonLocations.add(target);
                        seenNeutralArchons.remove(target);
                    }
                    target = location;
                    hasTarget = true;
                    isTargetNeutral = true;isTargetNArchon=false;isTargetPart=false;
                    foundNeutral=true;
                    seenNeutrals.add(location);
                    neutralArchonLocations.remove(location);
                }
                if(!foundNeutral) {
                    double leastRubble=Double.MAX_VALUE;
                    MapLocation pLocation=null;
                    for (MapLocation part : rc.sensePartLocations(rc.getType().sensorRadiusSquared)) {
                        //if(seenParts.contains(part))
                            //continue;
                        double rubble=rc.senseRubble(part);
                        if(rubble==0.0) {
                            RobotInfo xx=rc.senseRobotAtLocation(part);
                            if(xx==null){
                                pLocation=part;location=part;leastRubble=0.0;break;
                            }
                            else if(xx!=null && xx.type!=RobotType.TURRET) {
                                pLocation = part;
                                location = part;
                                leastRubble = 0.0;
                                break;
                            }
                        }
                        else{
                            if(rubble<leastRubble){
                                leastRubble=rubble;
                                pLocation=part;
                            }
                        }
                    }
                    if(leastRubble!=0.0 && partLocations.getSize()<20)
                        location=pLocation;
                    if(location!=null) {
                        if(targetExists && isTargetNeutral && target!=null){
                            neutralLocations.add(target);
                            seenNeutrals.remove(target);
                        }
                        else if(targetExists && isTargetPart && target!=null){
                            partLocations.add(target);
                            seenParts.remove(target);
                        }
                        else if(targetExists && isTargetNArchon && target!=null){
                            neutralArchonLocations.add(target);
                            seenNeutralArchons.remove(target);
                        }
                        target = location;
                        hasTarget = true;
                        isTargetNeutral = false;isTargetPart=true;isTargetNArchon=false;
                        seenParts.add(location);
                        partLocations.remove(location);
                    }

                }

            }
            if(rc.getLocation().equals(target)){
                hasTarget=false;
                isTargetNeutral = false;
                isTargetNArchon=false;
                isTargetPart=false;
            }
            else if(rc.isCoreReady() && (isTargetNeutral || isTargetNArchon) && rc.getLocation().isAdjacentTo(target)){
                hasTarget = false;
                isTargetNeutral = false;
                isTargetNArchon=false;
                isTargetPart=false;
                RobotInfo info=rc.senseRobotAtLocation(target);
                if(info!=null && info.team==Team.NEUTRAL) {
                    rc.activate(target);
                }
            }
            else if(rc.isCoreReady()){
                Nav.navigate(target);
                return;
            }
        }


        MapLocation location=null;

        location=findNearestLocation(neutralArchonLocations.getKeys(),seenNeutralArchons);
        if(location!=null){
            rc.setIndicatorString(1," Neutral target ");
            neutralArchonLocations.remove(location);
            seenNeutralArchons.add(location);
            hasTarget=true;target=location;
            isTargetNeutral=false;isTargetNArchon=true;isTargetPart=false;
            Nav.navigate(location);
            return;
        }

        location=null;

        for(RobotInfo info:rc.senseNearbyRobots()){
            if(info.team==Team.NEUTRAL) {
                location=info.location;
                break;
            }
        }

        if(location==null)
            location=findNearestLocation(neutralLocations.getKeys(),seenNeutrals);

        if(rc.isCoreReady() && location!=null) {
            rc.setIndicatorString(0,"Neutral target "+location);
            target=location;hasTarget=true;isTargetNeutral=true;isTargetNArchon=false;isTargetPart=false;
            seenNeutrals.add(location);
            neutralLocations.remove(location);
            Nav.navigate(location);
        }

        location=null;

        double leastRubble=Double.MAX_VALUE;
        MapLocation pLocation=null;
        for (MapLocation part : rc.sensePartLocations(rc.getType().sensorRadiusSquared)) {
            double rubble=rc.senseRubble(part);
            if(rubble==0.0) {
                pLocation = part;
                location=part;
                leastRubble=0.0;
                break;
            }
            else{
                if(rubble<leastRubble){
                    leastRubble=rubble;
                    pLocation=part;
                }
            }
        }
        if(leastRubble!=0.0 && partLocations.getSize()<20)
            location=pLocation;

        if(location==null)
            location=findNearestLocation(partLocations.getKeys(),seenParts);


        if(rc.isCoreReady() && location!=null) {
            rc.setIndicatorString(1,"Part target "+location);
            target=location;hasTarget=true;isTargetNeutral=false;isTargetNArchon=false;isTargetPart=true;
            seenParts.add(location);
            partLocations.remove(location);
            Nav.navigate(location);
        }
    }

    private static void repair() throws GameActionException {
        RobotInfo []ours=rc.senseNearbyRobots(rc.getType().attackRadiusSquared, rc.getTeam());
        for(RobotInfo our:ours) {
            if(our.type!=RobotType.ARCHON && our.health < our.type.maxHealth) {
                rc.repair(our.location);
                return;
            }
        }
    }


    private static void leaderElection() {
        if(rc.getType()==RobotType.ARCHON&&rc.getRoundNum()==0){
            int num_signals=0;
            Signal s;
            while((s=rc.readSignal())!=null){
                if(s.getTeam()==rc.getTeam())
                num_signals++;}
            num_archons = rc.getRobotCount();

            if(num_signals==0) {
                isLeader = true;
                //rc.setIndicatorDot(rc.getLocation().add(Direction.EAST,3),magneta[0],magneta[1],magneta[2]);
            }

            try {
                //TODO: check leader election, radius issue
                rc.broadcastSignal(rc.getType().sensorRadiusSquared*4);
            } catch (GameActionException e) {
                e.printStackTrace();
            }

        }
    }

    private static Direction randomDirection() {
        return Direction.values()[(int)(rnd.nextDouble()*8)];
    }
}
