package strat;
import battlecode.common.*;

import java.util.Map;

/**
 * Created by ps on 1/18/16.
 */
public class FastIntMap {
    private static final int HASH = Math.max(GameConstants.MAP_MAX_WIDTH,
            GameConstants.MAP_MAX_HEIGHT);
    private int size = 0;
    private int[][] has = new int[HASH][HASH];


    public void add(MapLocation loc,int z) {
        int x = loc.x % HASH;
        int y = loc.y % HASH;
        if (has[x][y]==0){
            size++;
            has[x][y] = z;
        }
        else{
            has[x][y]=z;
        }
    }

    public void remove(MapLocation loc) {
        int x = loc.x % HASH;
        int y = loc.y % HASH;
        if (has[x][y]!=0){
            size--;
            has[x][y] = 0;
        }
    }

    public boolean contains(MapLocation loc) {
        return has[loc.x % HASH][loc.y % HASH]!=0;
    }

    public int get(MapLocation loc) {
        return has[loc.x % HASH][loc.y % HASH];
    }


    public void clear() {
        has = new int[HASH][HASH];
        size = 0;
    }


}
