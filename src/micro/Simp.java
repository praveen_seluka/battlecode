package micro;

import battlecode.common.*;
import scala.tools.nsc.transform.patmat.Logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;

/**
 * Created by ps on 1/9/16.
 */
public class Simp {
    static Random rnd;
    static RobotController rc;
    static Direction []allDirections;

    static int DEFENSIVE = 10;

    static {
        int index=0;
        allDirections=new Direction[8];
        for(Direction d:Direction.values()) {
            if(d==Direction.NONE || d==Direction.OMNI) continue;
            else allDirections[index++]=d;
        }
    }
    static void setRandom(Random x) {
	    rnd=x;
    }
    static void setRC(RobotController rc2) {
        rc=rc2;
    }
    static Direction getDirectionCanMove() {
	    Direction possible[]=new Direction[8];
        int index=0;
        for(Direction d:allDirections) {
            if(rc.canMove(d)) {
               possible[index++]=d;
            }
        }
        if(index>0)
            return possible[rnd.nextInt(index)];
        else
            return null;

    }

    static boolean isDirectionSafe(Direction x) {
        //RobotInfo [] hostiles=rc.senseHostileRobots(rc.getLocation(),rc.getType().sensorRadiusSquared);
        RobotInfo []hostiles=RobotPlayer.hostileRobots;
        MapLocation next=rc.getLocation().add(x);
        boolean good=true;
        for(RobotInfo hostile:hostiles){
            if(!hostile.type.canAttack())
                continue;
            if(next.distanceSquaredTo(hostile.location) < hostile.type.attackRadiusSquared+DEFENSIVE) {
                good=false;
                break;
            }
        }
        return good;
    }

    //TODO: improve the algorithm, use heat maps kind-of
    static Direction getSafestDirection() {
        RobotInfo [] hostiles=rc.senseHostileRobots(rc.getLocation(),rc.getType().sensorRadiusSquared);
        Direction []goods=new Direction[8];
        int index=0;
        for(Direction d:allDirections) {
            if(!rc.canMove(d))
                continue;
            MapLocation next=rc.getLocation().add(d);
            boolean good=true;
            for(RobotInfo hostile:hostiles){
                if(next.distanceSquaredTo(hostile.location) < hostile.type.attackRadiusSquared+DEFENSIVE) {
                    good=false;
                    break;
                }
            }
            if(good) {
                goods[index++]=d;
            }

        }
        if(index>0)
            return  goods[rnd.nextInt(index)];
        else
            return  null;
    }

    static Direction getSafestDirection2() {
        //RobotInfo [] hostiles=rc.senseHostileRobots(rc.getLocation(),rc.getType().sensorRadiusSquared);
        RobotInfo []hostiles=RobotPlayer.hostileRobots;
        Direction []goods=new Direction[8];
        int aveX=0;
        int aveY=0;
        int numberOfHostiles=hostiles.length;
        for(RobotInfo hostile:hostiles) {
            if(hostile.type==RobotType.FASTZOMBIE) {
                aveX+=hostile.location.x;
                aveY+=hostile.location.y;
                numberOfHostiles++;
            }

            aveX+=hostile.location.x;
            aveY+=hostile.location.y;
        }
        int x=aveX/numberOfHostiles;
        int y=aveY/numberOfHostiles;
        MapLocation center=new MapLocation(x,y);
        Direction to=rc.getLocation().directionTo(center).opposite();
        rc.setIndicatorString(0,"enemy center "+center.toString());
        if(rc.canMove(to))
            return to;
        else {
            if(rc.canMove(to.rotateLeft()))
                return to.rotateLeft();
            else if(rc.canMove(to.rotateRight()))
                return to.rotateRight();
        }
        return getDirectionCanMove();
    }

    public static MapLocation findNearestInSet(HashSet<MapLocation> locations) throws GameActionException {
        if(locations.size()==0) throw new GameActionException(GameActionExceptionType.CANT_DO_THAT," null locations set");
        double leastDistance = Double.MAX_VALUE;
        MapLocation leastLocation = null;
        for(MapLocation location:locations) {
            double distance = rc.getLocation().distanceSquaredTo(location);
            if(distance<leastDistance) {
                leastDistance=distance;
                leastLocation=location;
            }
        }
        return  leastLocation;
    }

    public static MapLocation findCornerLocation(MapLocation[] ls) throws GameActionException {
                /*NORTH(0, -1),0
                NORTH_EAST(1, -1),1
                EAST(1, 0),2
                SOUTH_EAST(1, 1),3
                SOUTH(0, 1),4
                SOUTH_WEST(-1, 1),5
                WEST(-1, 0),6
                NORTH_WEST(-1, -1),7*/

        for(MapLocation l:ls) {
            boolean out[]=new boolean[8];
            if(rc.onTheMap(l)) {
                for (int i = 0; i < 8; i++) {
                    MapLocation test = l.add(Direction.values()[i]);
                    if (rc.canSense(test) && !rc.onTheMap(test)) {
                        out[i] = true;
                    }
                }

                if (out[7] && out[0] && out[6])
                    return l;
                else if (out[1] && out[0] && out[2])
                    return l;
                else if (out[3] && out[4] && out[2])
                    return l;
                else if (out[5] && out[4] && out[6])
                    return l;
            }
        }

        return null;
    }
}
