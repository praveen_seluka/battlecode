package improve;

import battlecode.common.*;
import scala.Array;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by ps on 1/8/16.
 */
public class RobotPlayer {
    static Random rnd;

    static RobotController rc = null;
    static boolean isLeader = false;
    static int [] magneta = new int[]{238,0,238};
    static int scouts_created = 0;
    static int num_archons = 0;

    static FastIterableSet neutralLocations;
    static FastIterableSet partLocations;
    static FastIterableSet denLocations;

    public static void run(RobotController xrc) {
        rc = xrc;
        rnd=new Random(rc.getID());
        Simp.setRandom(rnd);
        Simp.setRC(rc);
        scout_direction = randomDirection();
        neutralLocations=new FastIterableSet();
        partLocations=new FastIterableSet();
        denLocations=new FastIterableSet();


        leaderElection();
        opponent=Team.values()[(1-rc.getTeam().ordinal())];

        while(true) {

            Message.load();

            if(rc.getRoundNum()==0)
                Clock.yield();
            try {
                hostileRobots=rc.senseHostileRobots(rc.getLocation(), rc.getType().sensorRadiusSquared);
                zombieRobots=rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, Team.ZOMBIE);
                enemyRobots=rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, opponent);
                RobotInfo myRobots[]=rc.senseNearbyRobots(rc.getType().sensorRadiusSquared, rc.getTeam());
                ourRobots=new RobotInfo[myRobots.length+1];
                for(int i=0;i<myRobots.length;i++)
                    ourRobots[i]=myRobots[i];
                ourRobots[myRobots.length]=rc.senseRobotAtLocation(rc.getLocation());

                if (rc.getType() == RobotType.ARCHON) {
                    archon();
                } else if (rc.getType() == RobotType.SOLDIER || rc.getType()==RobotType.GUARD || rc.getType()==RobotType.VIPER) {
                    soldier();
                }
                else if(rc.getType() == RobotType.SCOUT) {
                    scout();
                }
            }catch (GameActionException e) {
                e.printStackTrace();
            }
            Clock.yield();
        }
    }

    static Direction scout_direction;
    private static void scout() throws GameActionException {

        if(hostileRobots.length>0) {
            int numAttackable=0;
            for(RobotInfo info:hostileRobots){
                if(info.type!=RobotType.ARCHON && info.type!=RobotType.SCOUT && info.type!=RobotType.ZOMBIEDEN)
                    numAttackable++;
            }
            if(numAttackable>0) {
                Direction to = Simp.getSafestDirection2();
                if (to != null && rc.isCoreReady()) {
                    rc.move(to);
                    scout_direction = to;
                    return;
                }
            }
        }
        //correct our previous assumptions
        //neutral, part, den may be gone. Then add new ones
        for(MapLocation loc:denLocations.getKeys()){
            if(rc.canSense(loc)){
                RobotInfo info=rc.senseRobotAtLocation(loc);
                if(info==null || info.type!=RobotType.ZOMBIEDEN)
                    denLocations.remove(loc);
            }
        }
        for(MapLocation loc:neutralLocations.getKeys()){
            if(rc.canSense(loc)){
                RobotInfo info=rc.senseRobotAtLocation(loc);
                if(info==null || info.team!=Team.NEUTRAL)
                    neutralLocations.remove(loc);
            }
        }
        for(MapLocation loc:partLocations.getKeys()){
            if(rc.canSense(loc)){
                RobotInfo info=rc.senseRobotAtLocation(loc);
                if(info==null || info.team!=Team.NEUTRAL)
                    partLocations.remove(loc);
            }
        }

        boolean ourArchoninView=false;

        for(RobotInfo info:rc.senseNearbyRobots()){
            if(info.team==Team.NEUTRAL)
                neutralLocations.add(info.location);
            else if(info.type==RobotType.ZOMBIEDEN)
                denLocations.add(info.location);
            else if(info.type==RobotType.ARCHON && info.team==rc.getTeam())
                ourArchoninView=true;

        }

        MapLocation lastLocation=null;
        for(MapLocation part:rc.sensePartLocations(rc.getType().sensorRadiusSquared)) {
            if(lastLocation==null)
                lastLocation=part;
            else if(lastLocation!=null){
                int distance=part.distanceSquaredTo(lastLocation);
                if(distance<20)
                    continue;
                else {
                    lastLocation=part;
                    partLocations.add(part);
                }
            }
        }

        MapLocation denLocation=findNearestLocation(denLocations.getKeys());

        if(denLocation!=null) {
            denLocations.remove(denLocation);
            Message.sendMessage(denLocation, MessageType.ZOMBIE_DEN, 600);
            rc.setIndicatorString(0,"Den "+denLocation);
        }

        MapLocation neutralLocation=findNearestLocation(neutralLocations.getKeys());

        if(neutralLocation!=null) {
            neutralLocations.remove(neutralLocation);
            Message.sendMessage(neutralLocation, MessageType.NEUTRAL, 600);
            rc.setIndicatorString(1, "neutral " + neutralLocation);
        }

        MapLocation partLocation=findNearestLocation(partLocations.getKeys());

        if(partLocation!=null) {
            partLocations.remove(partLocation);
            Message.sendMessage(partLocation, MessageType.PART, 600);
            rc.setIndicatorString(2, "Part " + partLocation);

        }


        boolean moved=false;
        if(rc.isCoreReady()) {
            rc.setIndicatorString(0,""+rc.getRoundNum()+" "+scout_direction);
            if(rc.canMove(scout_direction)) {
                rc.move(scout_direction);
                moved=true;
            }
            if(!moved) {
                Direction to=Simp.getDirectionCanMove();
                if(to!=null) {
                    rc.move(to);
                    scout_direction=to;
                }
            }
        }
    }

    private static MapLocation findNearestLocation(MapLocation[] locations) {
        double minDistance=Double.MAX_VALUE;
        MapLocation location=null;
        for(MapLocation den:locations){
            double dist=rc.getLocation().distanceSquaredTo(den);
            if(dist<minDistance){
                minDistance=dist;
                location=den;
            }
        }
        return location;
    }

    static RobotInfo zombieRobots[]=null;
    static RobotInfo enemyRobots[]=null;
    static RobotInfo ourRobots[]=null;
    static RobotInfo hostileRobots[]=null;
    static Team opponent;
    private static void soldier() throws GameActionException {

        if(zombieRobots.length>0 && enemyRobots.length==0) {
            if((zombieRobots.length*2)>ourRobots.length)
                rc.broadcastSignal(rc.getType().sensorRadiusSquared * 4);
            if(attackOrRetreat())
                return;
        }
        else if(enemyRobots.length>0){
            if((hostileRobots.length*2)>ourRobots.length)
                rc.broadcastSignal(rc.getType().sensorRadiusSquared*4);
            if(attackOrRetreat2())
                return;
        }

        for(RobotInfo info:zombieRobots){
            if(info.type==RobotType.ZOMBIEDEN){
                if(rc.isWeaponReady() && rc.canAttackLocation(info.location))
                    rc.attackLocation(info.location);
            }
        }
        if(!hasTarget){
            if(Message.helpLocation!=null) {
                rc.setIndicatorString(1, " soldier location target" + Message.helpLocation);
                double dist=rc.getLocation().distanceSquaredTo(Message.helpLocation);
                if(dist>8) {
                    moveNearThis(Message.helpLocation);
                    return;
                }
            }
        }
        if(!hasTarget) {
            MapLocation locations[]=denLocations.getKeys();
            MapLocation location=findNearestLocation(locations);
            if(location!=null) {
                //System.out.println("Got target "+location);
                rc.setIndicatorString(0, "" + location);
                target = location;
                hasTarget = true;
                denLocations.remove(location);
            }
        }

        if(rc.isCoreReady() && hasTarget) {
            double dist=rc.getLocation().distanceSquaredTo(target);

            if (dist < rc.getType().sensorRadiusSquared || rc.getLocation().equals(target)) {
                hasTarget = false;
            }
            else {
                //rc.broadcastSignal(rc.getType().sensorRadiusSquared*3);
                rc.setIndicatorString(0,""+target);
            }
            Nav.navigate(target);
        }
        else{
            if(rc.getRoundNum()%5<3) {
                Direction d=Simp.getDirectionCanMove();
                if(rc.isCoreReady() && d!=null && rc.canMove(d)) {
                    rc.move(d);
                    return;
                }
            }
            else{
                int sumX = 0;
                int sumY = 0;
                int aveX = 0;
                int aveY = 0;
                for (int i=0;i<ourRobots.length;i++) {
                    RobotInfo friend=ourRobots[i];
                    sumX += friend.location.x;
                    sumY += friend.location.y;
                }
                if (ourRobots.length != 0) {
                    aveX = sumX / ourRobots.length;
                    aveY = sumY / ourRobots.length;
                    if (moveNearThis(new MapLocation(aveX, aveY)))
                        return;
                }
            }
        }
    }

    static Direction moveableDirections[]=new Direction[]{
            Direction.EAST,Direction.WEST,
            Direction.NORTH,Direction.NORTH_EAST,
            Direction.SOUTH,Direction.SOUTH_EAST,
            Direction.NORTH_WEST,Direction.SOUTH_WEST};

    private static boolean attackOrRetreat() throws GameActionException {
        int numUnderAttack=0;
        int numL1=0;
        int numL2=0;
        int numL3=0;
        int thisLevel=0;

        for(int i=0;i<ourRobots.length;i++){
            if(ourRobots[i].type.attackPower==0.0)
                continue;
            int minLevel=10;
            for(int j=0;j<zombieRobots.length;j++){
                int dist=ourRobots[i].location.distanceSquaredTo(zombieRobots[j].location);
                int attackRadiusSquared=zombieRobots[j].type.attackRadiusSquared;
                int l1Radius=8;
                int l2Radius=18;
                if(zombieRobots[j].type==RobotType.RANGEDZOMBIE){
                    l1Radius=25;
                    l2Radius=41;
                }
                int level=10;
                if(dist<=attackRadiusSquared) {
                    level=0;
                }
                else if(dist<=l1Radius){
                    level=1;
                }
                else if(dist<=l2Radius){
                    level=2;
                }
                else{
                    level=3;
                }
                if(level<minLevel)
                    minLevel=level;
            }
            if(i==ourRobots.length-1){
                thisLevel=minLevel;
            }
            if(minLevel==0) numUnderAttack++;
            else if(minLevel==1) numL1++;
            else if(minLevel==2) numL2++;
            else numL3++;
        }
        rc.setIndicatorString(0,numUnderAttack+" "+numL1+" "+numL2+" "+numL3+" ");

        RobotInfo rangedZombies[]=findZombiesByType(zombieRobots, RobotType.RANGEDZOMBIE);
        //Should we go for attack ? all robots eval the same logic, so decision at same time
        if((numL1+numL2)>=zombieRobots.length){
            //check for ranged zombie

            RobotInfo weakRZombie[]=top3WeakestRangedZombie(rangedZombies);
            if(attackAnyPossible(weakRZombie))
                return true;

            if(attackAnyPossible(zombieRobots))
                return true;

            if(rc.isCoreReady() && weakRZombie.length>0){
                moveNearThis(weakRZombie[0].location);
            }

        }

        if(thisLevel<=1){
            for(Direction d:moveableDirections){
                boolean good=true;
                MapLocation newlocation=rc.getLocation().add(d);
                for(int i=0;i<zombieRobots.length;i++){
                    int distance=newlocation.distanceSquaredTo(zombieRobots[i].location);

                    int l1Radius=8;
                    if(zombieRobots[i].type==RobotType.RANGEDZOMBIE){
                        l1Radius=25;
                    }
                    if(thisLevel==1 && distance<=l1Radius){
                        good=false;
                        break;
                    }
                    else if(thisLevel==0 && distance<=zombieRobots[i].type.attackRadiusSquared){
                        good=false;
                        break;
                    }

                }
                if(good && rc.isCoreReady()&& rc.canMove(d)) {
                    rc.move(d);
                    return true;
                }
            }
            //in danger zone, but cant run away - so attack, ranged first -rest next
            RobotInfo sortedRangedZombie[]=top3WeakestRangedZombie(rangedZombies);
            if(attackAnyPossible(sortedRangedZombie))
                return true;
            if(attackAnyPossible(zombieRobots))
                return true;

        }
        else if(thisLevel==2) {
            if(attackAnyPossible(zombieRobots))
                return true;
        }
        else if(thisLevel>=3){
            if(attackAnyPossible(zombieRobots))
                return true;
            //move closer
            if(zombieRobots.length>0) {
                Direction center = directionTowardsCenter(zombieRobots);
                if (center != null && rc.isCoreReady() && rc.canMove(center)) {
                    rc.move(center);
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean attackOrRetreat2() throws GameActionException {
        int numUnderAttack=0;
        int numL1=0;
        int numL2=0;
        int numL3=0;
        int thisLevel=0;

        for(int i=0;i<ourRobots.length;i++){
            if(ourRobots[i].type.attackPower==0.0)
                continue;
            int minLevel=10;
            for(int j=0;j<enemyRobots.length;j++){
                int dist=ourRobots[i].location.distanceSquaredTo(enemyRobots[j].location);
                int attackRadiusSquared=enemyRobots[j].type.attackRadiusSquared;

                int l1Radius=getl1radius(enemyRobots[j].type);
                int l2Radius=getl2radius(enemyRobots[j].type);

                int level=10;
                if(dist<=attackRadiusSquared)
                    level=0;
                else if(dist<=l1Radius)
                    level=1;
                else if(dist<=l2Radius)
                    level=2;
                else
                    level=3;
                if(level<minLevel)
                    minLevel=level;
            }
            if(i==ourRobots.length-1){
                thisLevel=minLevel;
            }
            if(minLevel==0) numUnderAttack++;
            else if(minLevel==1) numL1++;
            else if(minLevel==2) numL2++;
            else numL3++;
        }
        rc.setIndicatorString(0,numUnderAttack+" "+numL1+" "+numL2+" "+numL3+" ");

        RobotInfo rangedZombies[]=findZombiesByType(zombieRobots, RobotType.RANGEDZOMBIE);
        //Should we go for attack ? all robots eval the same logic, so decision at same time
        if((numL1+numUnderAttack)>=enemyRobots.length){
            //check for ranged zombie

            //RobotInfo weakRZombie[]=top3WeakestRangedZombie(rangedZombies);
            //if(attackAnyPossible(weakRZombie))
             //   return true;

            if(attackAnyPossible(enemyRobots))
                return true;

        }

        if(thisLevel<=1){
            for(Direction d:moveableDirections){
                boolean good=true;
                MapLocation newlocation=rc.getLocation().add(d);
                for(int i=0;i<enemyRobots.length;i++){
                    int distance=newlocation.distanceSquaredTo(enemyRobots[i].location);


                    int l1Radius=getl1radius(enemyRobots[i].type);
                    if(thisLevel==1 && distance<=l1Radius){
                        good=false;
                        break;
                    }
                    else if(thisLevel==0 && distance<=enemyRobots[i].type.attackRadiusSquared){
                        good=false;
                        break;
                    }

                }
                if(good && rc.isCoreReady()&& rc.canMove(d)) {
                    rc.move(d);
                    return true;
                }
            }
            //in danger zone, but cant run away - so attack, ranged first -rest next
//            RobotInfo sortedRangedZombie[]=top3WeakestRangedZombie(rangedZombies);
//            if(attackAnyPossible(sortedRangedZombie))
//                return true;
            if(attackAnyPossible(enemyRobots))
                return true;

        }
        else if(thisLevel==2) {
            if(attackAnyPossible(enemyRobots))
                return true;
        }
        else if(thisLevel>=3){
            if(attackAnyPossible(enemyRobots))
                return true;
            //move closer
            if(enemyRobots.length>0) {
                Direction center = directionTowardsCenter(enemyRobots);
                if (center != null && rc.isCoreReady() && rc.canMove(center)) {
                    rc.move(center);
                    return true;
                }
            }
        }
        return false;
    }

    private static int getl2radius(RobotType type) {
        switch (type.ordinal()){
            case 0:
                return 18;
            case 1:
                return 18;
            case 2:
                return 41;
            case 3:
                return 18;
            case 4:
                return 18;
            case 5:
                return 18;
            case 6:
                return 41;
            case 7:
                return 18;
            case 8:
                return 50;
            case 9:
                return 50;
            case 10:
                return 50;
            default:
                return 18;
        }
    }

    private static int getl1radius(RobotType type) {
        switch (type.ordinal()){
            case 0:
                return 8;
            case 1:
                return 8;
            case 2:
                return 25;
            case 3:
                return 8;
            case 4:
                return 8;
            case 5:
                return 8;
            case 6:
                return 25;
            case 7:
                return 8;
            case 8:
                return 32;
            case 9:
                return 32;
            case 10:
                return 32;
            default:
                return 8;
        }

    }

    private static boolean attackAnyPossible(RobotInfo[] robots) throws GameActionException {
        for(int i=0;i<robots.length;i++){
            RobotInfo robot=robots[i];
            if(rc.isWeaponReady() && rc.canAttackLocation(robot.location)){
                rc.attackLocation(robot.location);
                return true;
            }
        }
        return false;
    }

    private static RobotInfo[] top3WeakestRangedZombie(RobotInfo robots[]) {
        double health[]=new double[robots.length];
        for(int i=0;i<robots.length;i++)
            health[i]=robots[i].health;
        for(int i=0;i<robots.length;i++){
            int minHealthIndex=i;
            double minHealth=health[i];
            for(int j=i+1;j<robots.length;j++){
                if(health[j]<minHealth){
                    minHealth=health[j];
                    minHealthIndex=j;
                }
            }
            double temp=health[i];RobotInfo temp1=robots[i];
            health[i]=health[minHealthIndex];robots[i]=robots[minHealthIndex];
            health[minHealthIndex]=temp;robots[minHealthIndex]=temp1;
        }
        return robots;
    }

    private static Direction directionTowardsCenter(RobotInfo[] robots) {
        Direction []goods=new Direction[8];
        int sumX=0;
        int sumY=0;
        for(int i=0;i<robots.length;i++){
            RobotInfo info=robots[i];
            sumX+=info.location.x;
            sumY+=info.location.y;
        }
        int aveX=sumX/robots.length;
        int aveY=sumY/robots.length;
        MapLocation center=new MapLocation(aveX,aveY);
        Direction to=rc.getLocation().directionTo(center);
        //rc.setIndicatorString(0,"enemy center "+center.toString());
        if(rc.canMove(to))
            return to;
        else {
            if(rc.canMove(to.rotateLeft()))
                return to.rotateLeft();
            else if(rc.canMove(to.rotateRight()))
                return to.rotateRight();
        }
        return null;
    }

    private static RobotInfo[] findZombiesByType(RobotInfo zombies[], RobotType type) {
        RobotInfo matches[]=new RobotInfo[zombies.length];
        int index=0;
        for(RobotInfo zombie:zombies) {
            if(zombie.type==type)
                matches[index++]=zombie;
        }
        RobotInfo m[]=new RobotInfo[index];
        for(int i=0;i<index;i++){
            m[i]=matches[i];
        }
        return m;
    }
    /*
     /*
        RobotInfo map[][]=new RobotInfo[(t*2)+1][(t*2)+1];
        int myX=rc.getLocation().x;
        int myY=rc.getLocation().y;
        for(RobotInfo hostile:hostileRobots){
            int x=hostile.location.x-myX;
            int y=hostile.location.y-myY;
            bfs(x,y,map);
        }

    int dx[]=new int[]{-1,0,1,1,1,0,-1,-1};
    int dy[]=new int[]{1,1,1,0,-1,-1,-1,0}
    private static void bfs(int x, int y, int[][] map) {
        boolean seen[][]=new boolean[map.length][map.length];
        Queue<MapLocation> queue=new LinkedList<>();
        queue.add(new MapLocation(x,y));
        map[x][y]=0;
        seen[x][y]=true;
        while(!queue.isEmpty()){
            MapLocation cur=queue.remove();
            for(int i=0;i<8;i++) {
                int newx=cur.x+dx[i];
                int newy=cur.y+dy[i];
                if(seen[newx][newy])
                    continue;
                else{
                    map[newx][new]
                }
            }
        }
    }
    */

    private static boolean moveNearThis(MapLocation location) throws GameActionException {
        Direction d=rc.getLocation().directionTo(location);
        if(rc.isCoreReady() && rc.canMove(d)) {
            rc.move(d);
            return true;
        }
        Direction left=d.rotateLeft();
        Direction right=d.rotateRight();
        if(rc.isCoreReady() && rc.canMove(left)) {
            rc.move(left);
            return true;
        }
        else if(rc.isCoreReady() && rc.canMove(right)) {
            rc.move(right);
            return true;
        }
        //if(rc.senseRubble(location)>0){
        //    rc.clearRubble(d);
        //}

        return false;
    }

    private static boolean runAwayIfEnemyAdjacent(RobotInfo[] hostiles) throws GameActionException {
        int num_adjacent=0;
        int sumX=0;
        int sumY=0;
        for(RobotInfo hostile:hostiles) {
            if(hostile.type==RobotType.ZOMBIEDEN)
                continue;
            if(rc.getLocation().distanceSquaredTo(hostile.location) <=2) {
                num_adjacent++;
                sumX=sumX+hostile.location.x;
                sumY=sumY+hostile.location.y;
            }
        }
        if(num_adjacent==0)
            return false;
        int aveX=sumX/num_adjacent;
        int aveY=sumY/num_adjacent;
        Direction to=rc.getLocation().directionTo(new MapLocation(aveX,aveY)).opposite();
        if(rc.isCoreReady() && rc.canMove(to)){
            rc.move(to);
            return true;
        }
        return false;
    }

    private static boolean enemyAdjacent(RobotInfo[] hostiles) {
        for(RobotInfo hostile:hostiles) {
            if(rc.getLocation().distanceSquaredTo(hostile.location) <=2) {
                return true;
            }
        }
        return false;
    }

    private static MapLocation weakestLocation(RobotInfo[] hostiles) throws GameActionException {
        double health=Double.MAX_VALUE;
        RobotInfo target=null;
        for(RobotInfo hostile:hostiles) {
            if(hostile.health<health) {
                health=hostile.health;
                target=hostile;
            }
        }
        if(target!=null)
            return target.location;
        else
            return null;
    }

    static MapLocation target=null;
    static boolean isTargetNeutral=false;
    static boolean hasTarget=false;
    public static void archon() throws GameActionException {
        Direction dest=randomDirection();
        repair();
        if(hostileRobots.length>0) {
            int numAttackable=0;
            for(RobotInfo info:hostileRobots){
                if(info.type!=RobotType.ARCHON && info.type!=RobotType.SCOUT && info.type!=RobotType.ZOMBIEDEN)
                    numAttackable++;
            }
            if(numAttackable>0) {
                rc.broadcastSignal(rc.getType().sensorRadiusSquared*6);
                Direction to = Simp.getSafestDirection2();
                if (to != null && rc.isCoreReady()) {
                    rc.move(to);
                    return;
                }
            }
        }


        if (rc.getRoundNum()%700==1 && rc.isCoreReady()) {
            for(Direction d:moveableDirections) {
                if(rc.canBuild(d,RobotType.SCOUT)) {
                    rc.build(d, RobotType.SCOUT);
                    return;
                }
            }
        }

        if (rc.isCoreReady()) {
            for(Direction d:moveableDirections) {
                if(rc.canBuild(d,RobotType.SOLDIER)) {
                    rc.build(d, RobotType.SOLDIER);
                    return;
                }
            }
        }

        if(hasTarget) {
            rc.setIndicatorString(2,rc.getRoundNum()+" "+target);

            int distance=rc.getLocation().distanceSquaredTo(target);
            boolean targetExists=true;
            if(rc.canSense(target)) {
                if (isTargetNeutral) {
                    RobotInfo info = rc.senseRobotAtLocation(target);
                    if (info == null || info.team != Team.NEUTRAL) {
                        targetExists = false;
                        hasTarget=false;
                    }

                } else {
                    if (rc.senseParts(target) > 0) {
                        targetExists = false;
                        hasTarget=false;
                    }
                }
            }
            if(!targetExists || distance>rc.getType().sensorRadiusSquared){
                MapLocation location=null;
                boolean foundNeutral=false;

                for(RobotInfo info:rc.senseNearbyRobots()){
                    if(info.team==Team.NEUTRAL) {
                        location=info.location;
                        break;
                    }
                }
                if(location!=null) {
                    target = location;
                    hasTarget = true;
                    isTargetNeutral = true;
                    foundNeutral=true;
                }
                if(!foundNeutral) {
                    for (MapLocation part : rc.sensePartLocations(rc.getType().sensorRadiusSquared)) {
                        location = part;
                        break;
                    }
                    if(location!=null) {
                        target = location;
                        hasTarget = true;
                        isTargetNeutral = false;
                    }

                }

            }

            if(rc.getLocation().equals(target)){
                hasTarget=false;
            }
            else if(rc.isCoreReady() && isTargetNeutral && rc.getLocation().isAdjacentTo(target)){
                //TODO: optimization to early detect target is not there
                hasTarget = false;
                isTargetNeutral = false;
                RobotInfo info=rc.senseRobotAtLocation(target);
                if(info!=null && info.team==Team.NEUTRAL) {
                    rc.activate(target);
                }
            }
            else if(rc.isCoreReady()){
                Nav.navigate(target);
                return;
            }
        }

        MapLocation location=null;

        for(RobotInfo info:rc.senseNearbyRobots()){
            if(info.team==Team.NEUTRAL) {
                location=info.location;
            }
        }

        if(location==null){
            location=findNearestLocation(neutralLocations.getKeys());
            if(location!=null)
                neutralLocations.remove(location);
        }
        if(rc.isCoreReady() && location!=null) {
            rc.setIndicatorString(0,"Neutral target "+location);
            target=location;hasTarget=true;isTargetNeutral=true;
            Nav.navigate(location);
        }

        for(MapLocation part:rc.sensePartLocations(rc.getType().sensorRadiusSquared)) {
            location=part;
            break;
        }
        if(location==null){
            location=findNearestLocation(partLocations.getKeys());
            if(location!=null)
                partLocations.remove(location);
        }

        if(rc.isCoreReady() && location!=null) {
            rc.setIndicatorString(1,"Part target "+location);
            target=location;hasTarget=true;isTargetNeutral=false;
            Nav.navigate(location);
        }
    }

    private static void repair() throws GameActionException {
        RobotInfo []ours=rc.senseNearbyRobots(rc.getType().attackRadiusSquared, rc.getTeam());
        for(RobotInfo our:ours) {
            if(our.type!=RobotType.ARCHON && our.health < our.type.maxHealth) {
                rc.repair(our.location);
                return;
            }
        }
    }


    private static void leaderElection() {
        if(rc.getType()==RobotType.ARCHON&&rc.getRoundNum()==0){
            int num_signals=0;
            while(rc.readSignal()!=null) num_signals++;
            num_archons = rc.getRobotCount();

            if(num_signals==0) {
                isLeader = true;
                //rc.setIndicatorDot(rc.getLocation().add(Direction.EAST,3),magneta[0],magneta[1],magneta[2]);
            }

            try {
                //TODO: check leader election, radius issue
                rc.broadcastSignal(1);
            } catch (GameActionException e) {
                e.printStackTrace();
            }

        }
    }

    private static Direction randomDirection() {
        return Direction.values()[(int)(rnd.nextDouble()*8)];
    }
}
